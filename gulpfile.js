var gulp = require('gulp'),
    concat = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin');
gulp.task('default', function () {
    console.log('ае');
});

gulp.task('scripts', function () {
    return gulp.src([ // Берем все необходимые библиотеки
        'assets/tpl/js/main.js',
        'assets/tpl/js/libs.js',
        'assets/tpl/js/common.js',
        'assets/tpl/js/bodyScrollLock.min.js',
    ])
        .pipe(concat('app.min.js')) // Собираем их в кучу в новом файле libs.min.js
        .pipe(uglify()) // Сжимаем JS файл
        .pipe(gulp.dest('assets/tpl/js')); // Выгружаем в папку app/js
});

gulp.task('minify-main-css', function () {
    return gulp.src([
        'assets/tpl/css/main.css',
        'assets/tpl/css/style.css',
        'assets/tpl/css/service-header.css'
    ])
        .pipe(cleanCSS({compatibility: '*'}))
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('assets/tpl/css/'));
});
gulp.task('minify-common-css', function () {
    return gulp.src([
        'assets/tpl/css/libs.css',
        'assets/tpl/css/events.css',
        'assets/tpl/css/print.css',
        'assets/tpl/css/banner_services.css',
        'assets/tpl/css/service-menu-block.css',
        'assets/tpl/css/subscribe-modal.css',
        'assets/tpl/css/spec-offer.css',
        'assets/tpl/css/list-benefits.css',
        'assets/tpl/css/inline-block.css'
    ])
        .pipe(cleanCSS({compatibility: '*'}))
        .pipe(concat('common.min.css'))
        .pipe(gulp.dest('assets/tpl/css/'));
});
//копирование и сжатие картинок
gulp.task('imagemin', function () {
    gulp.src([
        'assets/tpl/images-src/*.*',
        'assets/tpl/images-src/**/*.*'
    ]) //Выберем наши картинки
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            pngquant: true,
        }))
        .pipe(gulp.dest('assets/tpl/images-dist/')) //И бросим в build
});