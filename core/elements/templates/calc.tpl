{extends 'file:templates/base.tpl'}
{block 'CONTENT'}
    <!-- main content -->
    <div class="calc-main">
        <div class="container">
            <div class="calc-main__content">
                {if $_modx->resource.header_common}
                    {set $header_common = json_decode($_modx->resource.header_common, true)}
                    {foreach $header_common as $header_block}
                        {if $header_block@first}
                            <div class="calc-main__content__left-side">
                                {include 'bread'}
                                <h1 class="h3 calc-main__title">{$header_block.title?:$_modx->resource.pagetitle}</h1>
                                <h2 class="calc-main__subtitle">{$header_block.subtitle}</h2>
                                <p class='calc-main__info'>{$header_block.desc | notags}</p>
                            </div>
                            <div class="lazy calc-main__content__right-side" data-bg="url({$header_block.img | phpthumbon:"q=80&f=jpg"|image_optimize})">
                            </div>
                        {/if}
                    {/foreach}
                {/if}
            </div>
        </div>
    </div>
    <div class="calc-form">
        <form action="">
            <div class="container">
                {if $_modx->resource.allpagecalc}
                    <div class="calc-form__content">
                        <div class="calc-form__content__row">
                            <div class="calc-form__content__column">
                                <div class="input-group">
                                    <p class="block-title">Тип услуги</p>
                                    <select name="service-type">
                                        {set $type_services = json_decode($_modx->resource.allpagecalc, true)}
                                        {foreach $type_services as $type_service}
                                            <option value="{$type_service.name}" data-page="{$type_service.service}">{$type_service.name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="calc-form__content__column calc-form__afterload">
                                <div class="input-group">
                                    <p class="block-title">Вид услуги</p>
                                    <select name="service-view">
                                        <option value="" selected disabled>---</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                {/if}
            </div>
            <div class="calc-form__result">
                <div class="container">
                    <div class="calc-form__result__content">
                        <div class="calc-form__result__left-side">
                            <p>Стоимость услуги: <span id="result-val">123 456</span></p>
                            <p class="small">Стоимость фиксируется на 3 рабочих дня</p>
                        </div>
                        <div class="calc-form__result__right-side">
                            <a href="javascript:void(0);" class="question-btn-comm red-btn-style">Получить коммерческое</a>
                            <a href="#question_modal" class="question-btn btn-border-style modal-win">Задать вопрос</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="calc-info">
        <div class="container">
            <h2 class="h3 calc-info__title">{$_modx->resource.h2}</h2>
            <p class="h6 calc-info__subtitle">{$_modx->resource.introtext | notags}</p>
            <div class="calc-info__content">
                <div class="calc-info__content__left-side">
                    {$_modx->resource.content | getseotext}
                </div>
            </div>
        </div>
    </div>
    <!-- main content end-->
{/block}