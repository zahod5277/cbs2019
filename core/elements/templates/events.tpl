{extends 'file:templates/base.tpl'}
{block 'CONTENT'}
<div class="left-side-type">
    {include 'file:chunks/common/sidebar.tpl'}
    <div class="left-side-content">
        <div class="article-main">
            <div class="article-main__breadcrumbs-block">
                <div class="container">
                    {include 'file:chunks/common/breadcrumbs.tpl'}
                </div>
            </div>
            <div class="container">
                <div class="article-main__content">
                    <div class="article-main__content__left-side events__outer">
                        <div class="main-content" itemscope="">
                            <h1 class="article-main__title" itemprop="headline">{$_modx->resource.pagetitle}</h1>
                            <div class="events">
                                {if $_modx->resource.sidebar_event?}
                                    {var $events = $_modx->resource.sidebar_event|fromJSON}
                                    {foreach $events as $event}
                                        {var $date = $_modx->runSnippet('@FILE:snippets/formatDate.php',[
                                        'date' => $event.date,
                                        ])}
                                        <div class="lazy events__item" data-bg="url('{$event.img|image_optimize}')">
                                        <div class="events__info">
                                            <span class="events__item-type events__item-type--{$event.color}">{$event.type}</span>
                                            <span class="events__item--date">{$date}</span>
                                        </div>
                                        <div class="events__descr">
                                            <p class="events__item--title">{$event.title}</p>
                                            <p class="events__item--subtitle">{$event.desc}
                                        </div>
                                        {if $event.link?}
                                            <a href="{$event.link|url}" class="events__item-link">Подробнее</a>
                                        {/if}
                                    </div>
                                    {/foreach}
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}