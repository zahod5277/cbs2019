{extends 'file:templates/base.tpl'}
{block 'CONTENT'}
    <div class="error-main">
        <div class="container">
            <h1 class="h3 error-main__title">Не найдено</h1>
            <p class="error-main__subtitle">
                Простите, но у нас возникли проблемы <br> с поиском страницы, которую вы запрашиваете.
            </p>
            <div class="error-main__btns-row">
                <a onclick="javascript:history.back(); return false;" style="cursor: pointer;" class="backlink">Назад</a>
                <a href="/" class="main-page btn-style">На главную</a>
                <a href="{6439 | url}" class="main-page btn-style">Карта сайта</a>
            </div>
        </div>
    </div>
{/block}