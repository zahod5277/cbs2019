{extends 'file:templates/base.tpl'}
{block 'CONTENT'}
    <!-- main content -->
    <div class="left-side-type">
        {include 'file:chunks/common/sidebar.tpl'}
        <div class="left-side-content">
            <div class="practice-main">
                <div class="container">
                    {include 'file:chunks/common/breadcrumbs.tpl'}
                    <h1 class="h2 practice-main__title">Главные новости</h1>
                    {include 'file:chunks/infocenter/practice/infocenter.practice.main.tabs.tpl'}
                    <div class="practice-main__content">
                        {if $_modx->resource.id == 15}
                            {set $all_parents = $_modx->runSnippet('!pdoResources', ['parents' => 15, 'templates' => 8, 'limit' => 0, 'returnIds' => 1])}
                        {/if}
                        {var $idsx = $_modx->runSnippet('excludeResourcesArticles')}
                        {$_modx->runSnippet('!pdoPage', [
                            'cache' => 0,
                            'includeContent' => 1,
                            'includeTVs' => 'image,tags,video',
                            'resources' => '-2131,-2134,-126,-132,-130,-2136,-2132,-2135,-2133,-131,-127,-128,-129,-1985'~$idsx,
                            'parents' => $_modx->resource.id,
                            'sortby' => '{"publishedon":"DESC"}',
                            'where' => '{"class_key:=": "modDocument"}',
                            'showLog' => 0,
                            'fastMode' => 1,
                            'tpl' => '@FILE:chunks/infocenter/practice/infocenter.practice.row.tpl',
                            'limit' => 25,
                            'toPlaceholder' => 'cont',
                                'tplPagePrevEmpty' => '@INLINE ',
                                'tplPageNextEmpty' => '@INLINE ',
                                'tplPage' => '@INLINE <li class="pagination-list__item"><a class="pagination-list__link" href="[[+href]]">[[+pageNo]]</a></li>',
                                'tplPageWrapper' => '@INLINE <ul class="pagination-list">[[+prev]][[+pages]][[+next]]</ul>',
                                'tplPageActive' => '@INLINE <li class="pagination-list__item"><a class="pagination-list__link active" href="[[+href]]">[[+pageNo]]</a></li>',
                                'tplPagePrev' => '@INLINE <li class="pagination-list__item"><a class="pagination-list__link prev-arrow" href="[[+href]]"></a></li>',
                                'tplPageNext' => '@INLINE <li class="pagination-list__item"><a class="pagination-list__link next-arrow" href="[[+href]]"></a></li>'
                        ])}
                        {if $_modx->getPlaceholder('page') == 1}
                            {include 'file:chunks/infocenter/practice/infocenter.practice.main.news.tpl'}
                        {/if}
                        {$_modx->getPlaceholder('cont')}
                    </div>
                    <div class="pagination-block">
                        {$_modx->getPlaceholder('page.nav')}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main content end-->
{/block}
{block 'SCRIPTS'}
    {parent}
    <script>
        $.fn.chunk = function (size) {
            var arr = [];
            for (var i = 0; i < this.length; i += size) {
                arr.push(this.slice(i, i + size));
            }
            return this.pushStack(arr, "chunk", size);
        }

        $(".practice-main__content > .practice-main__content__block.text-block").chunk(2).wrap('<div class="half-block"></div>');
    </script>
{/block}