{extends 'file:templates/base.tpl'}
{block 'CONTENT'}
    {include 'file:chunks/main/main.section.tpl'}
    {include 'file:chunks/main/main.service.tpl'}
    {include 'file:chunks/main/main.about.tpl'}
    {include 'file:chunks/main/main.numbers.tpl'}
    {include 'file:chunks/main/main.advantage.tpl'}
    {include 'file:chunks/main/main.clients.tpl'}
    {include 'file:chunks/reviews/reviews.answer.slider.tpl'}
    {include 'file:chunks/main/main.centr.tpl'}
    {include 'file:chunks/main/main.private.tpl'}
{/block}