{extends 'file:templates/base.tpl'}
{block 'CONTENT'}
    <!-- main content -->
<div class="about-main">
    <div class="container">
        <div class="about-main__content">
        <div class="about-main__content__left-side">
            {include 'file:chunks/common/breadcrumbs.tpl'}
            <h1 class="h2 about-main__title">{$_modx->resource.pagetitle}</h1>
            <h2 class="h4 about-main__subtitle">{$_modx->resource.h2}</h2>
            <div class="about-main__info">
                {$_modx->resource.introtext}
            </div>
        </div>
        <div class="lazy about-main__content__right-side" data-bg="url('{'assets/tpl/images/about-main-image.jpg'|image_optimize}')">
            {var $video_present = ('video_present' | option)}
            {if $video_present}
                <a href="http://www.youtube.com/watch?v={$video_present}" class="video-link video-link-modal"><p>Видео о компании</p></a>
            {/if}
        </div>
        </div>
    </div>
</div>
{include 'file:chunks/about/about.team.tpl'}
{include 'file:chunks/about/about.numbers.tpl'}
{include 'file:chunks/about/about.principles.tpl'}
{include 'file:chunks/about/about.keypersons.tpl'}
{include 'file:chunks/services/services.ratings.tpl'}
{include 'file:chunks/reviews/reviews.answer.slider.tpl'}
{include 'file:chunks/main/main.clients.tpl'}
{include 'file:chunks/keyses/keyses.slider.tpl'}
<div class="about-search-block">
    <div class="container">
        <div class="about-search-block__content">
            <div class="about-search-block__content__left-side">
                <div class="descr-block">
                    <h2 class="about-search-block__title">Ищем крутых <br>специалистов</h2>
                    <p class="about-search-block__subtitle">Ищем хороших специалистов в области учета, налогов и финансов</p>
                    <a href="#anket_spec" class="about-search-block__btn btn-border-style white-style modal-win">Заполнить анкету</a>
                </div>
                <div class="image-block">
                    <img data-src="assets/tpl/images/search-person-image.png" alt="search-person" class="lazy search-image">
                </div>
            </div>
            <div class="about-search-block__content__right-side">
                <div class="image-block">
                    <img data-src="assets/tpl/images/search-person-image2.png" alt="search-person" class="lazy search-image">
                </div>
                <div class="descr-block">
                    <h2 class="about-search-block__title">Ищем надежных <br>партнеров</h2>
                    <p class="about-search-block__subtitle">Ищем надежных партнеров в смежных профессиональных сферах</p>
                    <a href="#anket_part" class="about-search-block__btn btn-border-style white-style modal-win">Заполнить анкету</a>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- main content end-->
{/block}