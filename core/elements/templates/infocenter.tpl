{extends 'file:templates/base.tpl'}
{block 'CONTENT'}
    <!-- main content -->
<div class="left-side-type">
    {include 'file:chunks/common/sidebar.tpl'}
    <div class="left-side-content">
        <div class="infocenter-main">
            <div class="container">
                <div class="infocenter-main__content">
                {if $_modx->resource.header_common}
                    {set $header_common = json_decode($_modx->resource.header_common, true)}
                    {foreach $header_common as $header_block}
                        {if $header_block@first}
                            <div class="infocenter-main__content__left-side">
                                {include 'file:chunks/common/breadcrumbs.tpl'}
                                <h1 class="h2 infocenter-main__title">{$header_block.title?:$_modx->resource.pagetitle}</h1>
                                <h2 class="h6 infocenter-main__subtitle">{$header_block.subtitle}</h2>
                            </div>
                            <div class="lazy infocenter-main__content__right-side" data-bg="url({$header_block.img | phpthumbon:"q=70&f=jpg"|image_optimize})">
                                {if $_modx->resource.videoID?}
                                <a href="http://www.youtube.com/watch?v={$_modx->resource.videoID}" class="video-link video-link-modal banner-video-link">
                                    <p class="banner-video-link__label">Видео о Центре Решений</p>
                                </a>
                                {/if}
                            </div>
                        {/if}
                    {/foreach}
                {/if}
                </div>
            </div>
        </div>
        <div class="infocenter-what">
            <div class="container">
                <h2 class="h3 infocenter-what__title">{$_modx->resource.h2}</h2>
                <p class="h6 infocenter-what__subtitle">{$_modx->resource.introtext | notags}</p>
                <div class="infocenter-what__content">
                    <div class="infocenter-what__content__left-side">
                        {$_modx->resource.content | getseotext}
                    </div>
                </div>
            </div>
        </div>
        <div class="infocenter-articles">
            <div class="container">
                <div class="infocenter-articles__content">
                {if $_modx->resource.infocenter_blocks}
                    {set $infocenter_blocks = json_decode($_modx->resource.infocenter_blocks, true)}
                    {foreach $infocenter_blocks as $infocenter_block}
                        <div class="infocenter-articles__content__block">
                            <div class="image-block">
                                <img data-src="{$infocenter_block.img | phpthumbon:"w=72&h=72&q=70&zc=T"|image_optimize}" alt="{$infocenter_block.title | htmlent}" class="lazy art-image">
                            </div>
                            <div class="descr-block">
                                <p class="block-title">{$infocenter_block.title}</p>
                                <div class="block-info">
                                    {$infocenter_block.desc}
                                </div>
                            </div>
                        </div>
                    {/foreach}
                {/if}
                </div>
            </div>
        </div>
        <div class="infocenter-adv">
            <div class="container">
                <div class="infocenter-adv__content">
                    <div class="infocenter-adv__content__block">
                        <div class="image-block"><img data-src="assets/tpl/images/svg/thesis-icon.svg" alt="3 года" class="lazy adv-image"></div>
                        <div class="descr-block">
                            <p>Проекту более 8 лет <br> и он бесплатный</p>
                        </div>
                    </div>
                    <div class="infocenter-adv__content__block">
                        <div class="image-block"><img data-src="assets/tpl/images/svg/200.svg" alt="200 выпусков" class="lazy adv-image"></div>
                        <div class="descr-block">
                            <p>Вышло более 200 <br> выпусков</p>
                        </div>
                    </div>
                    <div class="infocenter-adv__content__block">
                        <div class="image-block"><img data-src="assets/tpl/images/svg/articles.svg" alt="10000 публикаций" class="lazy adv-image"></div>
                        <div class="descr-block">
                            <p>Размещено более <br> 10 000 публикаций</p>
                        </div>
                    </div>
                    <div class="infocenter-adv__content__block">
                        <div class="image-block"><img data-src="assets/tpl/images/svg/followers.svg" alt="50000 подписчиков" class="lazy adv-image"></div>
                        <div class="descr-block">
                            <p>У нас более 50 000 <br> подписчиков</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="infocenter-subscribers">
            <div class="container">
                <div class="title-row">
                    <h2 class="h3 infocenter-subscribers__title">Отзывы подписчиков</h2>
                    <div class="infocenter-subscribers__btns">
                        <div class="answers-count">
                            <div class="slide-val">
                                <span class="current-slide"></span>
                                <span class="all-slides"></span>
                            </div>
                            <div class="slide-btns"></div>
                        </div>
                    </div>
                </div>
                <div class="infocenter-subscribers__content">
                    <div id="subscripe-slider" class="owl-carousel subscripe-slider">
                    {if $_modx->resource.answer_slider_presscenter}
                        {set $answer_slider_presscenter = json_decode($_modx->resource.answer_slider_presscenter, true)}
                        {foreach $answer_slider_presscenter as $answer_block}
                            <div class="slide-item">
                                <p class="company-name">{$answer_block.name}</p>
                                <p class="person-who">{$answer_block.rang}</p>
                                <p class="person-name">{$answer_block.fio}</p>
                                <div class="answer-info">
                                    {$answer_block.intro?:$answer_block.full}
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                    </div>
                    <div class="btn-row">
                        <a href="#answer_modal" class="btn-border-style modal-win infocenter-subscribers__btn">Оставить отзыв</a>
                    </div>
                </div>
            </div>
        </div>
        {include 'file:chunks/infocenter/infocenter.subscribe.tpl'}
    </div>
</div>
<!-- main content end-->
{/block}