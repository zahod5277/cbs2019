<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
{block 'HEAD'}
    {include 'file:chunks/common/head.tpl'}
{/block}
{block 'BODY'}
<body>
{/block}
{block 'HEADER'}
    {include 'file:chunks/common/header.tpl'}
{/block}
{block 'CONTENT'}
    
{/block}
{block 'FOOTER'}
    {include 'file:chunks/common/footer.tpl'}
{/block}
{block 'MODALS'}
    {include 'file:chunks/common/modals.tpl'}
{/block}
{block 'SCRIPTS'}
    {include 'file:chunks/common/scripts.tpl'}
{/block}
</body>
</html>