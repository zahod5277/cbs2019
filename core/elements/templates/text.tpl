{extends 'file:templates/base.tpl'}
{block 'BODY'}
<body class="simple-clean-body">
{/block}
{block 'CONTENT'}
<div class="left-side-type">
    <div class="left-side-content">
        <div class="article-main">
            <div class="article-main__breadcrumbs-block">
                <div class="container">
                    {include 'file:chunks/common/breadcrumbs.tpl'}
                </div>
            </div>
            <div class="container">
            <div class="article-main__content">
                <div class="article-main__content__left-side">
                    <div id="mainContent" class="main-content">
                        
                        <h1 class="article-main__title">{$_modx->resource.pagetitle}</h1>
                        <h2 class="h6">{$_modx->resource.h2}</h2>
                        <div class="main-content__info">
                            {if $_modx->resource.video}
                                <iframe style="width:100%;max-width:100%;height:400px" src="//www.youtube-nocookie.com/embed/{$_modx->resource.video | geturivideo}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            {/if}
                            {$_modx->resource.content}
                        </div>
                        
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
{/block}