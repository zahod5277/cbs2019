{extends 'file:templates/base.tpl'}
{block 'CONTENT'}
    {include 'file:chunks/services/service.tpl'}
{/block}
{block 'SCRIPTS'}
    {parent}
    <script>
        $.fn.chunk = function (size) {
            var arr = [];
            for (var i = 0; i < this.length; i += size) {
                arr.push(this.slice(i, i + size));
            }
            return this.pushStack(arr, "chunk", size);
        }

        $(".recommened-block.recommened-block--text").chunk(2).wrap('<div class="half-block"></div>');
    </script>
{/block}