{extends 'file:templates/base.tpl'}
{block 'CONTENT'}
    <!-- main content -->
    <div class="contacts-main">
        <div class="container">
            <div class="contacts-main__content">
                <div class="contacts-main__left-side">
                    <div class="contacts-main__title active" data-type="info">
                        <h1 class="h4">{$_modx->resource.pagetitle}</h1>
                        <p>Отвечаем в любом канале связи в течение 5 минут</p>
                        <a href="javascript:void(0);" class="contact-link" data-type="info"></a>
                    </div>
                    <div class="contacts-main__title" data-type="map">
                        <h2 class="h4">{$_modx->resource.h2}</h2>
                        <p>8 офисов в Московском регионе</p>
                        <a href="javascript:void(0);" class="contact-link" data-type="map">Открыть карту</a>
                    </div>
                </div>
                <div class="contacts-main__right-side" itemscope="" itemtype="http://schema.org/Organization">
                    <link itemprop="url" href="{'server_protocol' | option}://{'http_host' | option}">
                    <meta itemprop="logo" content="{'server_protocol' | option}://{'http_host' | option}/assets/tpl/images/svg/main-logo.svg">
                    <meta itemprop="name legalName" content="{'site_name' | option}">
                    <div class="contacts-main__type" style="display: block;" data-type="info">
                        <div class="info-row">
                            <div class="links-column">
                                <p class="block-name">Телефоны</p>
                                <div class="link-row"><a href="tel:{'phone1' | option | replace:" ":"" | replace:"(":"" | replace:")":"" | replace:"-":""}" itemprop="telephone" class="phone-link calltracking" content="{'phone1' | option | replace:" ":"" | replace:"(":"" | replace:")":"" | replace:"-":""}">{'phone1' | option}</a><span>многоканальный</span></div>
                                <div class="link-row"><a href="tel:{'phone2' | option | replace:" ":"" | replace:"(":"" | replace:")":"" | replace:"-":""}" itemprop="telephone" class="phone-link" content="{'phone2' | option | replace:" ":"" | replace:"(":"" | replace:")":"" | replace:"-":""}">{'phone2' | option}</a><span>мобильный</span></div>
                                <div class="email-block">
                                    <p class="block-name">E-mail</p>
                                    <a href="mailto:{'email' | option}" itemprop="email" class="mail-link emailtracking" content="{'email' | option}">{'email' | option}</a>
                                    <span>Ответим на ваше письмо в течение 5 минут</span>
                                </div>
                            </div>
                            <div class="messengers-column">
                                <p class="block-name">Мессенджеры</p>
                                <div class="messengers-list">
                                    <a href="https://api.whatsapp.com/send?phone={'whatsapp' | option}" class="messengers-link" target="_blank">
                                        <i class="contacts-icon contacts-icon--wa-icon">
                                        </i>
                                        WhatsApp
                                    </a>
                                    <a href="viber://pa?chatURI={'viber' | option}" target="_blank" class="messengers-link">
                                        <i class="contacts-icon contacts-icon--vb-icon">
                                        </i>
                                        Viber
                                    </a>
                                    <a href="https://tele.click/{'telegram' | option}" target="_blank" class="messengers-link">
                                        <i class="contacts-icon contacts-icon--tg-icon">
                                        </i>
                                        Telegram
                                    </a>
                                </div>
                                <p class="gray">Отвечаем онлайн</p>
                            </div>
                            <div class="social-column">
                                <p class="block-name">Социальные сети</p>
                                <ul class="social-list">
                                    <li class="social-list__item"><a href="{'insta' | option}" class="social-list__link inst-icon" target="_blank">Репорты в Instagram</a></li>
                                    <li class="social-list__item"><a href="{'fb' | option}" class="social-list__link fb-icon" target="_blank">Серьезный разговор
                                            <br>в Facebook</a></li>
                                    <li class="social-list__item"><a href="{'vk' | option}" class="social-list__link vk-icon" target="_blank">Дайджест в Вконтакте</a></li>
                                    <li class="social-list__item"><a href="{'tw' | option}" class="social-list__link tw-icon" target="_blank">Краткие новости в Twitter</a></li>
                                    <li class="social-list__item"><a href="{'tube' | option}" class="social-list__link yt-icon" target="_blank">Полезные видео
                                            <br>на канале YouTube</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="office-row">
                            {if $_modx->resource.office}
                                {set $offices = json_decode($_modx->resource.office, true)}
                                <div class="office-row__column">
                                    {foreach $offices as $office index=$off_idx}
                                        <div class="office-block">
                                            <p class="office-title">{$office.name}</p>
                                            <p class="office-city" style="min-height: 40px;">{$office.address_text?:$office.address}</p>
                                        </div>
                                        {if (($off_idx+1 - 2) % 2 === 0)}
                                        </div>
                                        <div class="office-row__column">
                                        {/if}
                                    {/foreach}
                                </div>
                            {/if}
                        </div>
                    </div>
                    <div class="contacts-main__type map-info" data-type="map">
                        <div class="map-content">
                            <div class="office-list">
                                {if $offices}
                                    {foreach $offices as $office index=$off_idx}
                                        <div class="office-list__row" data-point="{$off_idx+1}" itemprop="address location" itemscope itemtype="http://schema.org/PostalAddress">
                                            <p class="office-name">{$office.name}</p>
                                            <p class="office-adress" itemprop="addressLocality streetAddress">{$office.address_text?:$office.address}</p>
                                            {if $office.metro}
                                                <p class="office-metro">
                                                    <span class="color-list">
                                                        <span class="color" style="background-color: #aaa;"></span>
                                                    </span>
                                                    {$office.metro}
                                                </p>
                                            {/if}
                                            {if $office.info}
                                                <div class="office-metro-info">
                                                    {$office.info}
                                                </div>
                                            {/if}
                                        </div>
                                    {/foreach}
                                {/if}
                            </div>
                            <div class="map-block">
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}
{block 'SCRIPTS'}
    {parent}
    <script>
        function init() { 
            window.myMap = new ymaps.Map('map', { 
                center: [55.755814, 37.617635],
                controls: ['zoomControl', 'typeSelector', 'fullscreenControl', 'routeButtonControl'],
                zoom: 10
            });
            myMap.behaviors.disable("scrollZoom");

        {if $offices}
            {foreach $offices as $office}
                {if $office.address}
                ymaps.geocode('{$office.address}', { 
                    boundedBy: myMap.getBounds(),
                    results: 1
                }).then(function (res) { 
                    // Выбираем первый результат геокодирования.
                    var firstGeoObject = res.geoObjects.get(0),
                            // Координаты геообъекта.
                            coords = firstGeoObject.geometry.getCoordinates(),
                            // Область видимости геообъекта.
                            bounds = firstGeoObject.properties.get('boundedBy');
                    //firstGeoObject.options.set('preset', 'default#imageWithContent');
                    firstGeoObject.options.set('iconLayout', 'default#image');
                    firstGeoObject.options.set('iconImageSize', [150, 50]);
                    firstGeoObject.options.set('iconImageHref', '/assets/tpl/images/svg/main-logo.svg');
                    // Получаем строку с адресом и выводим в иконке геообъекта.
                    firstGeoObject.properties.set('iconCaption', '{'site_name' | option}');
                    firstGeoObject.properties.set('balloonContentBody', '{$office.address_text?:$office.address}');

                    // Добавляем первый найденный геообъект на карту.
                    myMap.geoObjects.add(firstGeoObject);

                });
                {/if}
            {/foreach}
        {/if}
        }
        if ($("div").is("#map")) {
            ymaps.ready(init);
        }
    </script>
{/block}