<?php
$events = $out = '';
$pdo = $modx->getService('pdoTools');

$ru_month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
$en_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

if ( $res = $modx->resource ) {
    if ( $relevant = $res->getTVvalue('sidebar_event') ) {
        $relevant = json_decode($relevant, true);
        $i = 1;
        foreach ($relevant as $serv) {
            if ( $i == 1 ) $idx = 'one';
            else $idx = 'two';
            $none = '';
            if ( !$link = $modx->makeUrl($serv['link']) ) {
                $none = 'd-none';
            }
            $desc = wordwrap($serv['desc'], 50, "<br />\n");
            $date = str_replace($en_month, $ru_month, date('d F Y в H:i', strtotime($serv['date'])));
            $events .= $pdo->getChunk($tpl,[
                'idx' => $idx,
                'color' => $serv['color'],
                'type' => $serv['type'],
                'date' => $date,
                'title' => $serv['title'],
                'img' => $serv['img'],
                'desc' => $desc,
                'link' => $link,
                'none' => $none
            ]);
            $i++;
        }
        return $events;
    }

    $parent = $res->parent;
    while ($parent > 0) {
        if ( $relevant = $modx->getObject('modResource', $parent)->getTVvalue('sidebar_event') ) {
            $relevant = json_decode($relevant, true);
            $i = 1;
            foreach ($relevant as $serv) {
                if ( $i == 1 ) $idx = 'one';
                else $idx = 'two';
                $none = '';
                if ( !$link = $modx->makeUrl($serv['link']) ) {
                    $none = 'd-none';
                }
                $desc = wordwrap($serv['desc'], 50, "<br />\n");
                $date = str_replace($en_month, $ru_month, date('d F Y в H:i', strtotime($serv['date'])));

                $events .= $pdo->getChunk($tpl,[
                    'idx' => $idx,
                    'color' => $serv['color'],
                    'type' => $serv['type'],
                    'date' => $date,
                    'title' => $serv['title'],
                    'img' => $serv['img'],
                    'desc' => $desc,
                    'link' => $link,
                    'none' => $none
                ]);
                $i++;
            }
            return $events;
        }
        $parent = $modx->getObject('modResource', $parent)->parent;
    }

}
return $events;