<?php
$ru_month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
$en_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

$formatted_date = str_replace($en_month, $ru_month, date('d F Y в H:i', strtotime($date)));

return $formatted_date;