<?php
###### вывод видов услуг по типу выбранной услуги ######

$pdo = $modx->getService('pdoTools');
//часть кода со сниппета getImageList для фильтрации по параметру where
//самому мне JSON фильтровать как-то лень
$migx = $modx->getService('migx', 'Migx', $modx->getOption('migx.core_path', null, $modx->getOption('core_path') . 'components/migx/') . 'model/migx/', $scriptProperties);
if (!($migx instanceof Migx))
    return '';
$migx->working_context = isset($modx->resource) ? $modx->resource->get('context_key') : 'web';
$where = '{"name":"' . $option . '"}';
$where = !empty($where) ? $modx->fromJSON($where) : array();
$tv = $modx->getObject('modTemplateVar', array('name' => 'allpagecalc'));
if (empty($outputvalue)) {
    $outputvalue = $tv->renderOutput(7);
    if (empty($outputvalue) && !empty($inheritFrom)) {
        foreach ($inheritFrom as $from) {
            if ($from == 'parents') {
                $outputvalue = $tv->processInheritBinding('', $docid);
            } else {
                $outputvalue = $tv->renderOutput($from);
            }
            if (!empty($outputvalue)) {
                break;
            }
        }
    }
}
$items = $modx->fromJSON($outputvalue);
$items = $migx->filterItems($where, $items);

$res = ['html' => '', 'success' => true];
$tpl = '<option value="[[+title]]" data-basePrice="[[+basePrice]]" [[+selected]]>[[+title]]</option>';
$i=1;

if(!is_array($items[0]['kind'])){
    $items[0]['kind'] = json_decode($items[0]['kind'], true);
}
if(is_array($items[0]['kind'])){
    foreach($items[0]['kind'] as $item){
        $selected = '';
        if($i == 1){
            $selected = 'selected';
        }
        $res['html'] .= str_replace(['[[+title]]', '[[+basePrice]]', '[[+selected]]'],
        [$item['title'], $item['basePrice'], $selected], $tpl);
        $i++;
    }
    return $res;
}
$res['success'] = false;
return $res;