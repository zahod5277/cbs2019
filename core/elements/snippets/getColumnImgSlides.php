<?php

$pdo = $modx->getService('pdoTools');

if (empty($images)) {
    return;
}


$portrait = [];
$landscape = [];

foreach ($images as $image) {
    list($width, $height) = getimagesize($image['img']);
    if ($width >= $height) {
        $landscape[] = [
            'alt' => $image['name'],
            'image' => $image['img']
        ];
    } else {
        $portrait[] = [
            'alt' => $image['name'],
            'image' => $image['img']
        ];
    }
}

$landscape = array_chunk($landscape, $rows);

//return $landscape;

$res = '';
$output = '';
$rows = [];
$i = 1;
foreach ($landscape as $l) {
    foreach ($l as $img) {
        $rows[$i] = $pdo->getChunk($tplRow, [
            'alt' => $img['alt'],
            'image' => $img['image']
        ]);
        $i++;
    }
}

$columns_chunk = array_chunk($rows, $columns);
//return $columns_chunk;
foreach ($columns_chunk as $chunk){
    $row = '';
    foreach ($chunk as $ch){
        $row .= $ch;
    }
    $cols[] = $pdo->getChunk($tpl,[
        'output' => $row
    ]);
}

$slides = array_chunk($cols, $columns);
return $slides;

//$output = $pdo->getChunk($tplOuter,[
//   'output' => $res
//]);

//$res = $rows;
//foreach ($portrait as $p) {
//    $row = '';
//    foreach ($p as $img) {
//        $row .= $pdo->getChunk($tplRow, [
//            'alt' => $img['alt'],
//            'image' => $img['image']
//        ]);
//    }
//    $res .= $pdo->getChunk($tpl, [
//        'output' => $row
//    ]);
//}
//$output = $res;
return $output;
