<?php
/*Активируем почтовый сервис MODX*/
$modx->getService('mail', 'mail.modPHPMailer');
$modx->mail->set(modMail::MAIL_FROM, $modx->getOption('emailsender'));
$modx->mail->set(modMail::MAIL_FROM_NAME, $modx->getOption('site_name'));
//$modx->mail->set(modMail::MAIL_ENCODING,'quoted-printable');
//тема письма
$subject = 'Статья из Инфо-Центра CBS Consalting Group';

/*Адрес получателя нашего письма получаем из заполненной формы*/
$email = $hook->getValue('emailt');
$articleID = $hook->getValue('article');
$page = $modx->getObject('modResource',$articleID);

$title = $page->get('pagetitle');
$content = $page->get('content');

$modx->mail->address('to', $email);

$placeholders = array(
    'title' => $title,
    'content' => $content
);

/*Заголовок сообщения*/
$modx->mail->set(modMail::MAIL_SUBJECT, $subject);

$modx->mail->set(modMail::MAIL_BODY,$modx->getChunk('articleMailContent',$placeholders));

/*Отправляем*/
$modx->mail->setHTML(true);
if (!$modx->mail->send()) {
    $modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$modx->mail->mailer->ErrorInfo);
}
$modx->mail->reset();