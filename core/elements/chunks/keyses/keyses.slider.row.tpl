<div class="slide-item" data-tags="{$_pls['tv.cases_resource']|join}" data-type="{$_pls['tv.cases_resource']|join}">
    <div class="case-block">
        <a href="{$uri}" title="{$_pls["tv.image"]["altTag"]?:$pagetitle | htmlent}">
            <div class="image-block">
                {if $_pls["tv.image"]}
                    <div class="lazy block-background" data-bg="url('{$_pls["tv.image"]["sourceImg"]["src"] | phpthumbon:"w=422&h=183&q=70&zc=C"}')">
                    </div>
                    {*<img src="{$_pls["tv.image"]["sourceImg"]["src"] | phpthumbon:"w=422&h=183&q=70&zc=C"}" alt="{$_pls["tv.image"]["altTag"]?:$pagetitle | htmlent}" class="service-case-image">*}
                {/if}
            </div>
            <div class="descr-block">
                <p class="block-title">{$pagetitle}</p>
                {$introtext?:"<p>"~($content | notags | stripmodxtags | truncate : 80 : "..." : true : true)~"</p>"}
            </div>
            <div class="info-block">
                <span class="category">
                    {($parent | resource:"menutitle")?:($parent | resource:"pagetitle")}
                    {if $_pls["tv.tags"]},
                        {foreach ($_pls["tv.tags"]|split) as $tag last=$last}
                        {if $last}{$tag}{else}{$tag}, {/if}
                        {/foreach}
                    {/if}
                </span>
                {var $date = $_modx->runSnippet('dateAgo',[
                    'input' => $publishedon,
                    'dateNow' => 0,
                    'dateFormat' => 'd F Y',
                    'dateDay' => 'd F Y',
                    'dateHours' => 0
                ])}
                <span class="date">{$date}</span>
            </div>
        </a>
    </div>
</div>