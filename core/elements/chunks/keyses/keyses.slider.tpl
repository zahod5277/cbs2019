<div class="cases-slider about-cases">
    <div class="container">
        <div class="title-row">
            <h2 class="h3 cases-slider__title">Кейсы</h2>
            <div class="cases-slider__btns">
                <div class="answers-type cases-slider__controls">
                    <a href="javascript:void(0);" data-keys-toggle data-tag="all" data-type="all" class="answer-type-link active">Все</a>
                    <a href="javascript:void(0);" data-keys-toggle data-tag="аудит" data-type="аудит" class="answer-type-link">Аудит</a>
                    <a href="javascript:void(0);" data-keys-toggle data-tag="бухучёт" data-type="бухучет" class="answer-type-link">Бухучет</a>
                    <a href="javascript:void(0);" data-keys-toggle data-tag="налоги" data-type="налоги" class="answer-type-link">Налоги</a>
                    <a href="javascript:void(0);" data-keys-toggle data-tag="консалтинг" data-type="консалтинг" class="answer-type-link">Консалтинг</a>
                    <a href="javascript:void(0);" data-keys-toggle data-tag="мсфо" data-type="мсфо" class="answer-type-link">МСФО</a>
                    <a href="javascript:void(0);" data-keys-toggle data-tag="оценка" data-type="оценка" class="answer-type-link">Оценка</a>
                    <a href="javascript:void(0);" data-keys-toggle data-tag="Автоматизация" data-type="Автоматизация" class="answer-type-link">Автоматизация</a>
                </div>
                <div class="answers-count">
                    <div class="slide-val">
                        <span class="current-slide"></span>
                        <span class="all-slides"></span>
                    </div>
                    <div class="slide-btns"></div>
                </div>
            </div>
        </div>
        <div class="service-cases__content">
            <div id="know-slider" class="owl-carousel servicecases-slider">
            {'pdoResources' | snippet : [
                'fastMode' => 1,
                'parents' => 121,
                'tpl' => '@FILE:chunks/keyses/keyses.slider.row.tpl',
                'includeTVs' => 'image,tags,cases_resource',
                'includeContent' => 1,
                'limit' => 100,
                'tplWrapper' => '@INLINE {$output}'
            ]}
            </div>
        </div>
        <div class="btn-row">
            <a href="{121 | url}" class="service-cases-link btn-border-style">Все кейсы</a>
        </div>
    </div>
</div>