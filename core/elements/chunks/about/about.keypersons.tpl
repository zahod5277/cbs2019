<div class="about-keypersons">
    <div class="container">
        <div class="about-keypersons__content">
            {if $_modx->resource.about_keypersons}
                {set $about_keypersons = json_decode($_modx->resource.about_keypersons, true)}
                {foreach $about_keypersons as $keypersons_block}
                    {if $keypersons_block@first}
                        <div class="title-row">
                            <h2 class="h3 about-keypersons__title">{$keypersons_block.title}</h2>
                            <div class="about-keypersons__btns">
                                <div class="answers-count">
                                    <div class="slide-val">
                                        <span class="current-slide"></span>
                                        <span class="all-slides"></span>
                                    </div>
                                    <div class="slide-btns"></div>
                                </div>
                            </div>
                        </div>
                        {set $personals = $personals_two = json_decode($keypersons_block.personals, true)}
                        <div id="keypersons-slider" class="keypersons-slider owl-carousel">
                            {foreach $personals as $persona index=$pers_idx}
                                {if $persona@first}
                                    <div id="s1" class="slide-item">
                                        <div class="slide-item__left-side">
                                            <div class="image-block">
                                                <!-- бол фото сотр справа -->
                                                <img data-src="{$persona.photo | phpthumbon:"w=425&h=560&q=85&zc=T&bg=ffffff"|image_optimize}" alt="{$persona.fio}" class="lazy keyperson-image">
                                            </div>
                                            <p class="name">{$persona.rang}</p>
                                            <p class="who">{$persona.fio}</p>
                                        </div>
                                        <div class="slide-item__right-side">
                                            <div class="persons-row">
                                                {foreach $personals_two as $persona_two}
                                                    {if !$persona_two@first}
                                                        <a href="javascript:void(0);" class="person-link" data-slide="{$persona_two@index}">
                                                            <!-- мал фото сотр -->
                                                            <img data-src="{$persona_two.photo | phpthumbon:"w=144&h=160&q=80&zc=T&bg=ffffff"|image_optimize}" alt="small-person" class="lazy person-image-small">
                                                        </a>
                                                    {/if}
                                                {/foreach}
                                            </div>
                                            <div class="slide-info">
                                                {$persona.desc}
                                            </div>
                                        </div>
                                        <div class="slide-item__docs">
                                            <p class="slide-title">{$persona.deviz}</p>
                                            {set $images = json_decode($persona.images, true)}
                                            {if $images}
                                                <div class="small-images-slider owl-carousel">
                                                    {foreach $images as $img}
                                                        <div class="slide-item">
                                                            <a href="{$img.img}" title="{$img.name}" class="slide-link">
                                                                <!-- фото дипломов дир-ра -->
                                                                <img data-src="{$img.img | phpthumbon:"w=184&h=162&q=80&bg=f1f6ff"}" alt="{$img.name}" class="lazy slide-image">
                                                            </a>
                                                        </div>
                                                    {/foreach}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                {else}
                                    <div class="slide-item">
                                        <div class="slide-item__left-side">
                                            <div class="image-block">
                                                <img data-src="{$persona.photo | phpthumbon:"w=425&h=560&q=85&zc=T&bg=ffffff"|image_optimize}" alt="{$persona.fio}" class="lazy keyperson-image">
                                            </div>
                                            <p class="name">{$persona.rang}</p>
                                            <p class="who">{$persona.fio}</p>
                                        </div>
                                        <div class="slide-item__right-side">
                                            <div class="persons-row">
                                                {foreach $personals_two as $persona_two}
                                                    {if $persona_two@index != $pers_idx}
                                                        <a href="javascript:void(0);" class="person-link" data-slide="{$persona_two@index}">
                                                            <img data-src="{$persona_two.photo | phpthumbon:"w=144&h=160&q=80&zc=T&bg=ffffff"|image_optimize}" alt="small-person" class="lazy person-image-small">
                                                        </a>
                                                    {/if}
                                                {/foreach}
                                            </div>
                                            <div class="slide-info">
                                                {$persona.desc}
                                            </div>
                                        </div>
                                        <div class="slide-item__docs">
                                            <p class="slide-title">{$persona.deviz}</p>
                                            {set $images = json_decode($persona.images, true)}
                                            {if $images}
                                                <div class="small-images-slider owl-carousel">
                                                    {foreach $images as $img}
                                                        <div class="slide-item">
                                                            <a href="{$img.img}" title="{$img.name}" class="slide-link">
                                                                <img data-src="{$img.img | phpthumbon:"w=184&h=162&q=80&bg=f1f6ff"|image_optimize}" alt="{$img.name}" class="lazy slide-image">
                                                            </a>
                                                        </div>
                                                    {/foreach}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                {/if}
                            {/foreach}
                        </div>
                    {/if}
                {/foreach}
            {/if}
        </div>
    </div>
</div>