<div class="main-numbers lazy" data-bg="url({'assets/tpl/images/main-numbers-bg.jpg'|phpthumbon:'w=800&h=400'|image_optimize})">
    <div class="container">
        {if (1 | resource:"adv_main")}
            {set $adv_main = json_decode((1 | resource:"adv_main"), true)}
            {foreach $adv_main as $adv_block}
                {if $adv_block@first}
                    <h2 class="h3 main-numbers__title">{$adv_block.title}</h2>
                    {set $adv_top = json_decode($adv_block.top, true)}
                    <div class="main-numbers__content">
                        {foreach $adv_top as $top}
                            {if $top@first}
                                <div class="main-numbers__content__left-side">
                                    <div class="numb-block">{$top.main}</div>
                                    <div class="numb-descr">{$top.desc}</div>
                                </div>
                            {else}
                                <div class="main-numbers__content__right-side">
                                    <div class="numb-block">{$top.main}</div>
                                    <div class="numb-descr">{$top.desc}</div>
                                </div>
                            {/if}
                        {/foreach}
                    </div>

                    {set $adv_middle = json_decode($adv_block.middle, true)}
                    <div class="main-numbers__top">
                        {foreach $adv_middle as $middle}
                            <div class="main-numbers__top__block">
                                <div class="numb-block">{$middle.main}</div>
                                <div class="numb-descr">{$middle.desc}</div>
                            </div>
                        {/foreach}
                    </div>

                    {set $adv_bottom = json_decode($adv_block.bottom, true)}
                    <div class="main-numbers__bottom">
                        {foreach $adv_bottom as $bottom}
                            <div class="main-numbers__bottom__block">
                                <div class="numb-block">{$bottom.main}</div>
                                <div class="numb-descr">{$bottom.desc}</div>
                            </div>
                        {/foreach}
                    </div>
                {/if}
            {/foreach}
        {/if}
    </div>
</div>