<div class="about-team">
    <div class="container">
        <div class="about-team__content">
            {if $_modx->resource.about_team}
                {set $about_team = json_decode($_modx->resource.about_team, true)}
                {foreach $about_team as $about_team_block}
                    {if $about_team_block@first}
                        <div class="lazy about-team__content__left-side" data-bg="url({$about_team_block.img | phpthumbon:"q=70&f=jpg"|image_optimize})">
                        </div>
                        {set $about_team_items = json_decode($about_team_block.items, true)}
                        <div class="about-team__content__right-side">
                            {foreach $about_team_items as $about_team_item}
                                <div class="about-team-block">
                                    <p><strong>{$about_team_item.title}</strong></p>
                                    {$about_team_item.desc}
                                </div>
                            {/foreach}
                        </div>
                    {/if}
                {/foreach}
            {/if}
        </div>
    </div>
</div>