
<script>
    (function (w, d, u) {
        var s = d.createElement('script')
        s.async = true
        s.src = u + '?' + (Date.now() / 60000 | 0)
        var h = d.getElementsByTagName('script')[0]
        h.parentNode.insertBefore(s, h)
    })(window, document, 'https://bitrix.cbscg.ru/upload/crm/site_button/loader_2_p6lkv4.js')
    window.addEventListener('onBitrixLiveChat', function(event)
    {
        var widget = event.detail.widget;

        // Установка дополнительных данных (публикуется при начале новой сессии, расширенный формат)
        widget.setCustomData([

            { "GRID": [
                    {
                        "NAME" : "Код Roistat",
                        "VALUE" : $.cookie('roistat_visit'),
                        "COLOR" : "#ff0000",
                        "DISPLAY" : "LINE"
                    },
                ]}
        ]);

    });
</script>
<link rel="stylesheet" href="/assets/tpl/css/common.min.css?ver=1.7303">
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter22051156 = new Ya.Metrika({
                    id: 22051156,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div>
        <img src="https://mc.yandex.ru/watch/22051156" style="position:absolute; left:-9999px;" alt="" />
    </div>
</noscript>
<!-- /Yandex.Metrika counter -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-48458331-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){ dataLayer.push(arguments); }
    gtag('js', new Date());

    gtag('config', 'UA-48458331-1');
</script>
<!-- JS -->
<script src="/assets/tpl/js/app.min.js?ver=4.4217"></script>
<script src="https://api-maps.yandex.ru/2.1/?apikey=f3833868-df29-404b-8d6a-b7761e50c0cc&lang=ru_RU"></script>
<script src="/assets/components/ajaxform/js/lib/jquery.form.min.js"></script>
<script src="/assets/components/ajaxform/js/lib/jquery.jgrowl.min.js"></script>
<script src="/assets/tpl/js/cookie.js"></script>
<script>
    $(function () {
        // Антиспам
        $('.client_form').append('<input type="text" name="org" value="" class="_org" style="visibility:hidden; height: 0; width: 0; padding: 0; border:none;"/>')
        // Антиспам х
    })
    $(function () {
        // Антиспам
        $('.email_form').append('<input type="text" name="org" value="" class="_org" style="visibility:hidden; height: 0; width: 0; padding: 0; border:none;"/>')
        // Антиспам х
    })
</script>
{if $_modx->resource.template == 10}
    {ignore}
        <script>
            jQuery(document).ready(function ($) {
                var cookieOptions = {expires: 1, path: '/'};
                var resource_parent = null;
                if (typeof cbsConfig != 'undefined') {
                    // plugin regcbsConfig
                    resource_parent = cbsConfig.resource_parent
                }

                var visites = $.cookie('visites');
                var isShow = true
                if (visites === undefined) {
                    visites = []
                } else {
                    visites = visites.split(',');
                    var resource_id;
                    for (var i = 0; i < visites.length; i++) {
                        if (!visites.hasOwnProperty(i)) {
                            continue
                        }
                        resource_id = parseInt(visites[i]);
                        if (resource_id === resource_parent) {
                            isShow = false
                        }
                    }
                }

                if (isShow) {
                    setTimeout(function () {
                        visites.push(resource_parent);
                        var tmp = visites.join(',');
                        $.cookie('visites', tmp, cookieOptions);
                        $.magnificPopup.open({
                            items: {
                                src: $('#seminar-modal'), // может быть HTML строкой, jQuery объектом, или CSS селектором
                            },
                            type: 'inline',
                            removalDelay: 300
                        })
                    }, 20000)
                }
            }) // end ready
        </script>
    {/ignore}
{/if}
<script src="https://cdn.jsdelivr.net/npm/intersection-observer@0.7.0/intersection-observer.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@13.0.1/dist/lazyload.min.js"></script>
<script>
    // Set the options to make LazyLoad self-initialize
    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
    });
</script>
<script>
    $( document ).ready(function() {
        $('.main-menu').css('transform','translateY(0px)');
        lazyLoadInstance.update();
    });
</script>
<script>
    var elems = document.getElementsByTagName('input');
    for (var i = 0; i < elems.length; i++) {
        elems[i].addEventListener('click', func);
    }
    function func() {
        var roi = $.cookie('roistat_visit')
        let inputs = document.getElementsByName( "roistat" );
        for( let i = 0; i < inputs.length; i++) {
            inputs[i].value = roi;
        }
    }
</script>
