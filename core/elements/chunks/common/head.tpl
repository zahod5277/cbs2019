<head>
    <!-- meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <base href="{'server_protocol' | option}://{'http_host' | option}">
    <link rel="canonical" href="{'server_protocol' | option}://{'http_host' | option}/{if $_modx->config.site_start != $_modx->resource.id}{$_modx->resource.uri}{/if}">

    <title>{($_modx->resource.longtitle?:$_modx->resource.pagetitle) | htmlent}</title>
    {if $_modx->resource.template == 9}
    <meta name="description" content="{$_modx->resource.pagetitle | htmlent}-{$_modx->resource.parent | resource : 'parent' | resource : 'pagetitle'}-[[*id:pdofield=`{ "top":2,"field":"longtitle"}`]]-{$_modx->config.site_name}">
    {else}
    <meta name="description" content="{$_modx->resource.description | htmlent}">
    {/if}
    <!-- social img -->
    <meta property="og:title" content="{($_modx->resource.longtitle?:$_modx->resource.pagetitle) | htmlent}" />
	<meta property="og:description" content="{$_modx->resource.description | htmlent}" />
	<meta property="og:url" content="{if $_modx->resource.id == $_modx->config.site_start}{$_modx->config.site_url}{else}{$_modx->resource.id | url : ['scheme' => 'full']}{/if}" />
	<meta property="og:image" content="{'server_protocol' | option}://{'http_host' | option}{($_modx->resource.image?:'/assets/tpl/images/svg/main-logo.svg')}" />
	<meta property="vk:image" content="{'server_protocol' | option}://{'http_host' | option}{($_modx->resource.image?:'/assets/tpl/images/svg/main-logo.svg')}" />
	<meta property="og:site_name" content="{'site_name' | option | htmlent}" />
	<meta property="og:type" content="article" />
    <!-- favicon -->
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#002766">
	<meta name="theme-color" content="#ffffff">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){ w[l]=w[l]||[];w[l].push({ 'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TNJNC7T');</script>
<!-- End Google Tag Manager -->
<!-- CSS -->
<link rel="stylesheet" href="/assets/tpl/css/app.min.css?ver=4.821">

<style>@media (max-width: 1440px){ 
.article-main__content__right-side .article-news-block.case-two { 
    -webkit-background-size: 80%;
    background-size: 68%;
    background-position: 148% 140px;}
    .article-main__content__right-side .article-news-block.case-one { 
    -webkit-background-size: 70%;
    background-size: 70%;
    background-position: 200% 120%; }}</style>
</head>