{if $id == 8}
<li class="mmenu-list__item">
    <a href="{'9'|url}" title="{$pagetitle}" class="mmenu-list__link" itemprop="url" {$attributes}>{$menutitle}</a>
</li>
{else}
<li class="mmenu-list__item">
    <a href="{$link}" title="{$pagetitle}" class="mmenu-list__link"  itemprop="url" {$attributes}>{$menutitle}</a>
</li>
{/if}