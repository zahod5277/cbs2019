{if $class_key=='modWebLink'}
    <li class="service-menu-block__subcat-outer">
        <a href="#" data-collapse="submenu" class="service-menu__subcat-item" title="{$pagetitle}"
           itemprop="url" {$attributes}>
            <span>{$menutitle}</span>
        </a>
        <ul>
            {$wrapper}
        </ul>
    </li>
{else}
    <li class="mmenu-list__item">
        <a href="{$link}" title="{$pagetitle}" class="mmenu-list__link"  itemprop="url" {$attributes}>{$menutitle}</a>
    </li>
{/if}