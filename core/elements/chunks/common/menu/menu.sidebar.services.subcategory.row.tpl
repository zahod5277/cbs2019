{if $class_key=='modWebLink'}
    <li class="submenu__list-item-outer">
        <a href="#" data-collapse="submenu" class="submenu__list__link submenu__list-link--sub" title="{$pagetitle}"
           itemprop="url" {$attributes}>
            <span>{$menutitle}</span>
        </a>
        <ul>
            {$wrapper}
        </ul>
    </li>
{else}
    <li class="submenu__list__item">
        <a href="{$link}" title="{$pagetitle}" class="submenu__list__link"  itemprop="url" {$attributes}>{$menutitle}</a>
    </li>
{/if}