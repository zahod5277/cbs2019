<div class="service-menu-block__column">
    <p class="column-title">
        <a class="title-link" href="{$link}" itemprop="url" {$attributes}>{$menutitle}</a>
    </p>
    <ul>{$wrapper}</ul>
</div>