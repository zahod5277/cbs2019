<a id="thanks_link" href="#thanks_modal" class="modal-win d-none"></a>
<a id="thanks_link2" href="#thanks_modal2" class="modal-win d-none"></a>
<a id="thanks_link3" href="#thanks_modal3" class="modal-win d-none"></a>
<a id="thanks_link4" href="#thanks_modal4" class="modal-win d-none"></a>
<a id="thanks_link_webinar" href="#thanks_webinar" class="modal-win d-none"></a>
<a id="thanks_link_presentation" href="#thanks_presentation" class="modal-win d-none"></a>
<div id="thanks_modal" class="zoom-anim-dialog mfp-hide modal-form-style thanks_modal">
    <div class="modal-body">
        <div class="modal-title">Спасибо за заявку!</div>
        <div class="modal-subtitle">Ваша заявка принята. <br>Мы свяжемся с вами в течение 10 минут.</div>
        <div class="modal-info">
            <p>А пока вы можете посмотреть наши соцсети.</p>
            <p class="small">Там мы публикуем новости, полезные материалы, <br> разбираем кейсы и отвечаем на вопросы.</p>
            <ul class="social-list">
                <li class="social-list__item"><a href="{'insta' | option}" class="social-list__link inst-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'fb' | option}" class="social-list__link fb-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'vk' | option}" class="social-list__link vk-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'tw' | option}" class="social-list__link tw-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'tube' | option}" class="social-list__link yt-icon" target="_blank"></a></li>
            </ul>
        </div>
    </div>
</div>
<div id="thanks_modal2" class="zoom-anim-dialog mfp-hide modal-form-style thanks_modal">
    <div class="modal-body">
        <div class="modal-title">Спасибо!</div>
        <div class="modal-subtitle">
            <p>Анкета успешно отправлена.</p>
            <p>Мы свяжемся с вами в течение дня.</p>
        </div>
        <div class="modal-info">
            <p>А пока вы можете посмотреть наши соцсети.</p>
            <p class="small">Там мы публикуем новости, полезные материалы, <br> разбираем кейсы и отвечаем на вопросы.</p>
            <ul class="social-list">
                <li class="social-list__item"><a href="{'insta' | option}" class="social-list__link inst-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'fb' | option}" class="social-list__link fb-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'vk' | option}" class="social-list__link vk-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'tw' | option}" class="social-list__link tw-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'tube' | option}" class="social-list__link yt-icon" target="_blank"></a></li>
            </ul>
        </div>
    </div>
</div>
<div id="thanks_modal3" class="zoom-anim-dialog mfp-hide modal-form-style thanks_modal">
    <div class="modal-body">
        <div class="modal-title">Спасибо!</div>
        <div class="modal-subtitle">
            <p>Статья успешно отправлена.</p>
            <p>Рекомендуйте нас коллегам и друзьям.</p>
        </div>
    </div>
</div>
<div id="thanks_modal4" class="zoom-anim-dialog mfp-hide modal-form-style thanks_modal">
    <div class="modal-body">
        <div class="modal-title">Спасибо!</div>
        <div class="modal-subtitle">
            <p>Вы успешно оформили подписку на нашу еженедельную новостную рассылку.</p>
            <p>Рекомендуйте нас коллегам и друзьям.</p>
        </div>
    </div>
</div>

<div id="thanks_webinar" class="zoom-anim-dialog mfp-hide modal-form-style thanks_modal">
    <div class="modal-body">
        <div class="modal-title">Спасибо за заявку!</div>
        <div class="modal-subtitle">Мы пришлем вам ссылку на вебинар за 1 час до его начала.</div>
        <div class="modal-info">
            <p>А пока вы можете посмотреть наши соцсети.</p>
            <p class="small">Там мы публикуем новости, полезные материалы, <br> разбираем кейсы и отвечаем на вопросы.</p>
            <ul class="social-list">
                <li class="social-list__item"><a href="{'insta' | option}" class="social-list__link inst-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'fb' | option}" class="social-list__link fb-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'vk' | option}" class="social-list__link vk-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'tw' | option}" class="social-list__link tw-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'tube' | option}" class="social-list__link yt-icon" target="_blank"></a></li>
            </ul>
        </div>
    </div>
</div>

<div id="thanks_presentation" class="zoom-anim-dialog mfp-hide modal-form-style thanks_modal">
    <div class="modal-body">
        <div class="modal-title">Спасибо!</div>
        <div class="modal-subtitle">
            <p>Презентация придет вам на почту в течение 10 минут.</p>
        </div>
        <div class="modal-info">
            <p>А пока вы можете посмотреть наши соцсети.</p>
            <p class="small">Там мы публикуем новости, полезные материалы, <br> разбираем кейсы и отвечаем на вопросы.</p>
            <ul class="social-list">
                <li class="social-list__item"><a href="{'insta' | option}" class="social-list__link inst-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'fb' | option}" class="social-list__link fb-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'vk' | option}" class="social-list__link vk-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'tw' | option}" class="social-list__link tw-icon" target="_blank"></a></li>
                <li class="social-list__item"><a href="{'tube' | option}" class="social-list__link yt-icon" target="_blank"></a></li>
            </ul>
        </div>
    </div>
</div>

<div id="email_modal" class="zoom-anim-dialog mfp-hide modal-form-style email_modal">
    <div class="modal-body">
        <div class="modal-title">Отправить на email</div>
        <div class="modal-info">
            {'!AjaxForm' | snippet : [
            'frontend_css' => 0,
            'snippet' => 'FormIt',
            'hooks' => 'csrf,email,sendArticle',
            'form' => 'email_form_tpl',
            'formSelector' => 'email_form',
            'emailTo' => ('e-mail'|option),
            'emailFrom' => "noreply@{$_modx->config.http_host}",
            'validate' => 'emailt:required,email:blank',
            'emailTpl' => 'articleMail',
            'emailSubject' => 'Статья на сайте '~$_modx->config.http_host,
            'clearFieldsOnSuccess' => true,
            'emailUseFieldForSubject' => 1,
            'formFields' => 'emailt',
            'fieldNames' => 'emailt==E-mail',
            'formName' => 'Отправка статьи клиенту',
            'validationErrorMessage' => 'Вы не указали свой e-mail адрес',
            'successMessage' => '',
            'subject' => 'Отправка статьи клиенту'
            ]}
        </div>
    </div>
</div>
<div id="send_to_email_modal" class="zoom-anim-dialog mfp-hide modal-form-style email_modal">
    <div class="modal-body">
        <div class="modal-title">Отправить на email</div>
        <div class="modal-info">
            {*manager_mail*}
            {'!AjaxForm' | snippet : [
            'frontend_css' => 0,
            'snippet' => 'FormIt',
            'hooks' => 'spam,validate,csrf,email,sendPDF',
            'form' => 'email_presentation_form_tpl',
            'formSelector' => 'email_form',
            'emailTo' => ('e-mail'|option),
            'emailFrom' => "noreply@{$_modx->config.http_host}",
            'validate' => 'emailt:required,email:blank',
            'emailTpl' => 'presentationMail',
            'emailSubject' => 'Отправлена презентация',
            'clearFieldsOnSuccess' => true,
            'formName' => 'Отправка презентации клиенту',
            'validationErrorMessage' => 'Вы не указали свой e-mail адрес',
            'successMessage' => '',
            'subject' => 'Отправка презентации клиенту'
            ]}
        </div>
    </div>
</div>
<div id="client_modal" class="zoom-anim-dialog mfp-hide modal-form-style client_modal">
    <div class="modal-body">
        <div class="modal-info">
            <div class="modal-title">Стать клиентом</div>
            <div class="modal-subtitle">Оставьте заявку и мы свяжемся с вами <br> в течение 10 минут</div>
        </div>
        {'!AjaxForm' | snippet : [
        'frontend_css' => 0,
        'snippet' => 'FormIt',
        'hooks' => 'spam,csrf,validate,modBitrixLead,FormItSaveForm',
        'form' => 'client_form_tpl',
        'formSelector' => 'client_form',
        'emailTo' => $_modx->config['e-mail'],
        'emailFrom' => "noreply@{$_modx->config.http_host}",
        'validate' => 'phone_f:required,email:blank',
        'emailTpl' => 'message-discounts',
        'emailSubject' => 'Заявка с сайта '~$_modx->config.http_host,
        'clearFieldsOnSuccess' => true,
        'emailUseFieldForSubject' => 1,
        'btxFieldnames' => '{"phone_f":"PHONE","name_f":"NAME", "message_f": "COMMENTS", "title": "TITLE","roistat": "UF_CRM_1565004282"}',
        'formFields' => 'phone_f,name_f,message_f',
        'fieldNames' => 'phone_f==Телефон,name_f==Имя,message_f==Сообщение',
        'formName' => 'Форма Стать клиентом',
        'validationErrorMessage' => 'Вы не указали свой телефон',
        'successMessage' => 'Заявка отправлена, мы с вами свяжемся'
        ]}
    </div>
</div>
<div id="seminar-modal" class="zoom-anim-dialog mfp-hide modal-form-style article__form seminar-box seminar" style="margin: 0 auto;">
    <div class="modal-body">
                    <div class="seminar__date">
                        Акция
                    </div>
                    <div class="seminar__title">
                        {var $categories = [
                        25,
                        5962,
                        5976,
                        5996,
                        6002,
                        6004,
                        41,

                        ]}
                        {foreach $categories as $key}
                            {if $_modx->resource.id == $key || $_modx->resource.parent == $key}
                                {'pdofield' | snippet : [
                                'id' => $key,
                                'field' => 'baner_title',
                                ]}
                            {/if}
                        {/foreach}

{*                        {$_modx->resource.baner_title}*}
                    </div>
                    {'!AjaxForm' | snippet : [
                    'frontend_css' => 0,
                    'snippet' => 'FormIt',
                    'hooks' => 'spam,csrf,modBitrixLead,FormItSaveForm',
                    'form' => 'service_baner',
                    'formSelector' => 'client_form',
                    'emailTo' => $_modx->config['e-mail'],
                    'emailFrom' => "noreply@{$_modx->config.http_host}",
                    'validate' => 'phone_f:required',
                    'emailTpl' => 'message-discounts',
                    'emailSubject' => 'Заявка с сайта '~$_modx->config.http_host,
                    'clearFieldsOnSuccess' => true,
                    'emailUseFieldForSubject' => 1,
                    'btxFieldnames' => '{"phone_f":"PHONE","name_f":"NAME", "title": "TITLE","roistat": "UF_CRM_1565004282"}',
                    'formFields' => 'phone_f,name_f,message_f',
                    'fieldNames' => 'phone_f==Телефон,name_f==Имя,message_f==Сообщение',
                    'formName' => 'Банер на странице услуг',
                    'validationErrorMessage' => 'Вы не указали свой телефон',
                    'successMessage' => 'Заявка отправлена, мы с вами свяжемся'
                    ]}
    </div>
</div>
<div id="offer_modal" class="zoom-anim-dialog mfp-hide modal-form-style client_modal">
    <div class="modal-body">
        <div class="modal-info">
            <div class="modal-title">Коммерческое <br> предложение</div>
            <div class="modal-subtitle">Перезвоним через 5 минут и отправим коммерческое предложение</div>
        </div>
        {'!AjaxForm' | snippet : [
        'frontend_css' => 0,
        'snippet' => 'FormIt',
        'hooks' => 'spam,csrf,modBitrixLead,FormItSaveForm',
        'form' => 'kp',
        'formSelector' => 'offer_form',
        'emailTo' => $_modx->config['e-mail'],
        'emailFrom' => "noreply@{$_modx->config.http_host}",
        'validate' => 'phone_f:required,email:blank',
        'emailTpl' => 'message-discounts',
        'btxFieldnames' => '{"phone_f":"PHONE","name_f":"NAME", "title": "TITLE","roistat": "UF_CRM_1565004282"}',
        'emailSubject' => 'Заявка с сайта '~$_modx->config.http_host,
        'clearFieldsOnSuccess' => true,
        'emailUseFieldForSubject' => 1,
        'formFields' => 'phone_f,name_f',
        'fieldNames' => 'phone_f==Телефон,name_f==Имя',
        'formName' => 'Форма Коммерческое предложение',
        'validationErrorMessage' => 'Вы не указали свой телефон',
        'successMessage' => 'Заявка отправлена, мы с вами свяжемся'
        ]}

    </div>
</div>

<div id="question_modal" class="zoom-anim-dialog mfp-hide modal-form-style question_modal">
    <div class="modal-body">
        <div class="modal-title">Как вы хотите задать вопрос?</div>
        <div class="modal-subtitle">Выберите для себя наиболее удобный способ связи</div>
        <div class="modal-info">
            <ul class="type-list">
                <li class="type-list__item"><a href="tel:{'phone1' | option | preg_replace : '/[^0-9+]/' : ''}" class="type-list__link phone-icon calltracking">{'phone1' | option}</a></li>
                <li class="type-list__item"><a href="tel:{'phone2' | option | preg_replace : '/[^0-9+]/' : ''}" class="type-list__link mobile-icon">{'phone2' | option}</a></li>
                <li class="type-list__item"><a href="mailto:{'email' | option}" class="type-list__link mail-icon emailtracking">{'email' | option}</a></li>
                <li class="type-list__item"><a href="https://api.whatsapp.com/send?phone={'whatsapp' | option}" class="type-list__link wa-icon">WhatsApp</a></li>
                <li class="type-list__item"><a href="viber://pa?chatURI={'viber' | option}" class="type-list__link vb-icon">Viber</a></li>
                <li class="type-list__item"><a href="https://tele.click/{'telegram' | option}" class="type-list__link tg-icon">Telegram</a></li>
            </ul>
        </div>
    </div>
</div>
<div id="answer_modal" class="zoom-anim-dialog mfp-hide modal-form-style client_modal">
    <div class="modal-body">
        <div class="modal-info">
            <div class="modal-title">Оставить отзыв</div>
            <div class="modal-subtitle">Оставьте отзыв и мы разместим его на сайте </div>
        </div>
        {'!AjaxForm' | snippet : [
        'frontend_css' => 0,
        'snippet' => 'FormIt',
        'hooks' => 'csrf,email,FormItSaveForm',
        'form' => 'answer_form_tpl',
        'formSelector' => 'offer_form',
        'emailTo' => $_modx->config['e-mail'],
        'emailFrom' => "noreply@{$_modx->config.http_host}",
        'validate' => 'name_f:required, message_f:required,email:blank',
        'emailTpl' => 'offer_form',
        'emailSubject' => 'Заявка с сайта '~$_modx->config.http_host,
        'clearFieldsOnSuccess' => true,
        'emailUseFieldForSubject' => 1,
        'formFields' => 'name_f,names_f,compname_f,message_f',
        'fieldNames' => 'name_f==Имя, names_f==Должность,compname_f==Название компании,message_f==Сообщение',
        'formName' => 'Форма Оставить отзыв',
        'validationErrorMessage' => 'Проверьте заполнненность полей',
        'successMessage' => 'Заявка отправлена, мы с вами свяжемся'
        ]}
    </div>
</div>

<div id="anket_spec" class="zoom-anim-dialog mfp-hide modal-form-style client_modal">
    <div class="modal-body">
        <div class="modal-info">
            <div class="modal-title">Анкета для специалиста</div>
            <div class="modal-subtitle">Отправьте форму и мы с вами свяжемся в течение дня</div>
        </div>
        {'!AjaxForm' | snippet : [
        'frontend_css' => 0,
        'snippet' => 'FormIt',
        'hooks' => 'csrf,email,FormItSaveForm',
        'form' => 'ankets_form_tpl',
        'formSelector' => 'ankets_form',
        'emailTo' => $_modx->config['e-mail'],
        'emailFrom' => "noreply@{$_modx->config.http_host}",
        'validate' => 'phone_f:required,email:blank',
        'emailTpl' => 'message-discounts',
        'emailSubject' => 'Заявка с сайта '~$_modx->config.http_host,
        'clearFieldsOnSuccess' => true,
        'emailUseFieldForSubject' => 1,
        'formFields' => 'phone_f,name_f,message_f',
        'fieldNames' => 'phone_f==Телефон,name_f==Имя,message_f==Сообщение',
        'formName' => 'Форма Анкета для специалиста',
        'validationErrorMessage' => 'Вы не указали свой телефон',
        'successMessage' => 'Заявка отправлена, мы с вами свяжемся'
        ]}
    </div>
</div>
<div id="anket_part" class="zoom-anim-dialog mfp-hide modal-form-style client_modal">
    <div class="modal-body">
        <div class="modal-info">
            <div class="modal-title">Анкета для партнера</div>
            <div class="modal-subtitle">Отправьте форму и мы с вами свяжемся в течение дня</div>
        </div>
        {'!AjaxForm' | snippet : [
        'frontend_css' => 0,
        'snippet' => 'FormIt',
        'hooks' => 'csrf,email,FormItSaveForm',
        'form' => 'anketp_form_tpl',
        'formSelector' => 'anketp_form',
        'emailTo' => $_modx->config['e-mail'],
        'emailFrom' => "noreply@{$_modx->config.http_host}",
        'validate' => 'phone_f:required,email:blank',
        'emailTpl' => 'message-discounts',
        'emailSubject' => 'Заявка с сайта '~$_modx->config.http_host,
        'clearFieldsOnSuccess' => true,
        'emailUseFieldForSubject' => 1,
        'formFields' => 'phone_f,name_f,message_f',
        'fieldNames' => 'phone_f==Телефон,name_f==Имя,message_f==Сообщение',
        'formName' => 'Форма Анкета для партнера',
        'validationErrorMessage' => 'Вы не указали свой телефон',
        'successMessage' => 'Заявка отправлена, мы с вами свяжемся'
        ]}
    </div>
</div>
<div id="offer_modal2" class="zoom-anim-dialog mfp-hide modal-form-style client_modal">
    <div class="modal-body">
        <div class="modal-info">
            <div class="modal-title">Коммерческое <br> предложение</div>
            <div class="modal-subtitle">Перезвоним через 5 минут и отправим коммерческое предложение</div>
        </div>
        {'!AjaxForm' | snippet : [
        'frontend_css' => 0,
        'snippet' => 'FormIt',
        'hooks' => 'spam,csrf,modBitrixLead,FormItSaveForm',
        'form' => 'offer_form2_tpl',
        'formSelector' => 'offer_form2',
        'emailTo' => $_modx->config['e-mail'],
        'emailFrom' => "noreply@{$_modx->config.http_host}",
        'validate' => 'phone_f:required,email:blank',
        'emailTpl' => 'message-discounts','btxFieldnames' => '{"phone_f":"PHONE","name_f":"NAME", "calcVal1,calcVal2,calcVal3,calcVal4,calcVal5,calcVal6,calcVal6,calcVal8,calcVal9,calcVal10,calcVal11,calcVal12, calcSumm": "COMMENTS", "title": "TITLE","roistat": "UF_CRM_1565004282"}',
        'emailSubject' => 'Заявка с сайта '~$_modx->config.http_host,
        'clearFieldsOnSuccess' => true,
        'emailUseFieldForSubject' => 1,
        'formFields' => 'phone_f,name_f,calcVal1,calcVal2,calcVal3,calcVal4,calcVal5,calcVal6,calcVal6,calcVal8,calcVal9,calcVal10,calcVal11,calcVal12, calcSumm',
        'fieldNames' => 'phone_f==Телефон,name_f==Имя, calcVal1==Тип услуги,calcVal2==Вид услуги,calcVal3==Вид деятельности,calcVal4==Количество сотрудников,calcVal5==Годовой оборот,calcVal6==Система налогообложения,calcVal6==Учет импортных операций,calcVal8==Много авансовых счетов,calcVal9==Подготовка архива первычных документов,calcVal10==Оптимизация налогообложения,calcVal11==Сдача отчетности,calcVal12==Общение с налоговыми органами,calcSumm==Стоимость услуги',
        'formName' => 'Форма Коммерческое предложение (Калькулятор)',
        'validationErrorMessage' => 'Вы не указали свой телефон',
        'successMessage' => 'Заявка отправлена, мы с вами свяжемся'
        ]}
    </div>
</div>