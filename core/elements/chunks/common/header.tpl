<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TNJNC7T"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="header">
    <div class="main-menu" style="transform: translateY(-300px);">
        <div class="container">
            <div class="menu-logo">
                <a href="/" class="logo-link">
                    <img class="logo-image" src="/assets/tpl/images/svg/main-logo.svg" alt="{'site_name' | option}">
                </a>
            </div>
            <div class="menu-block" itemscope itemtype="https://www.schema.org/SiteNavigationElement">
                <ul class="mmenu-list">
                    <li class="mmenu-list__item">
                        <a href="javascript:void(0);" class="mmenu-list__link service-link-main">Услуги</a>
                    </li>
                {$_modx->runSnippet('pdoMenu', [
                    'cache' => 1,
                    'fastMode' => 1,
                    'resources' => '-1','-6439',
                    'parents' => 0,
                    'level' => 2,
                    'tplOuter' => '@INLINE {$wrapper}',
                    'tpl' => '@FILE:chunks/common/menu/menu.item.row.tpl',
                ])}
                </ul>
            </div>
            <div class="backcall-block">
                <a href="tel:{'phone1' | option | preg_replace : '/[^0-9+]/' : ''}" class="header-phone-link calltracking">{'phone1' | option}</a>
                <a href="{7 | url}" class="cost-btn btn-border-style">Расчет стоимости</a>
                <a href="#question_modal" class="modal-win backcall-btn btn-style" rel="nofollow">Связаться с нами</a>
            </div>
            <button class="hamburger hamburger--slider" type="button">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
</header>
<!-- Modal windows -->
<div class="service-main-menu">
    <div class="service-menu-block" itemscope itemtype="https://www.schema.org/SiteNavigationElement">
        <a href="javascript:void(0);" class="close-service-menu"></a>
        {'pdoMenu' | snippet : [
            'cache' => 1,
            'cacheTime' => 604800,
            'fastMode' => 0,
            'parents' => 23,
            'tplOuter' => '@INLINE {$wrapper}',
            'tplCategoryFolder' => '@FILE:chunks/common/menu/menu.header.services.subcategory.row.tpl',
            'tpl' => '@FILE:chunks/common/menu/menu.header.services.parent.row.tpl',
            'tplInnerRow' => '@FILE:chunks/common/menu/menu.header.services.row.tpl',
            'level' => 3
        ]}
    </div>
</div>
<div class="print-header">
    <div class="print-header__logo">
        <div class="print-header__logo-image">
            <img src="/assets/Centrresheniy_images/main-logo[1].png" alt="Логотип версия для печати">
        </div>
        <div class="print-header__logo-text">
            <p>Аудит</p>
            <p>Учёт</p>
            <p>Налоги</p>
            <p>Консалтинг</p>
        </div>
    </div>
    <div class="print-header__contacts">
        <p>{'phone1'|option}</p>
        <p>info@cbscg.ru</p>
        <p>www.cbscg.ru</p>
    </div>
</div>