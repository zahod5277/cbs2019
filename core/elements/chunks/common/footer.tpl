<div class="print-footer">
    <img src="/assets/Centrresheniy_images/cbsicons.png" alt="">
    #cbsgroup
</div>
<footer class="footer">
    <div class="container">
        <div class="footer__content">
            <div class="footer__content__block">
                <p class="block-title">Услуги</p>
                <ul class="footer-list">
                    <li class="footer-list__item">
                        <a href="{25 | url}" class="footer-list__link">Аудиторские</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{5962 | url}" class="footer-list__link">Бухгалтерские</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{5976 | url}" class="footer-list__link">Налоговые</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{5996 | url}" class="footer-list__link">Консалтинговые</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{6002 | url}" class="footer-list__link">МСФО</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{41 | url}" class="footer-list__link">Автоматизация 1С</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{6004 | url}" class="footer-list__link">Оценка</a>
                    </li>
                </ul>
            </div>
            <div class="footer__content__block">
                <p class="block-title">Информация</p>
                <ul class="footer-list">
                    <li class="footer-list__item">
                        <a href="{4 | url}" class="footer-list__link">О компании</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{6 | url}" class="footer-list__link">Контакты</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{9 | url}" class="footer-list__link">Центр решений</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{26 | url}" class="footer-list__link">Private Service</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{27 | url}" class="footer-list__link">Мероприятия</a>
                    </li>
                    <li class="footer-list__item">
                        <a href="{6439 | url}" class="footer-list__link">Карта сайта</a>
                    </li>
                </ul>
            </div>
            <div class="footer__content__block">
                <p class="block-title">Подписывайтесь на рассылку.</p>
                <div class="send-form" id="mc_embed_signup">
                    <p class="form-title">Новости финансов, налогов и учета.</p>
                    <form id="subscribe_form" action="https://cabinet.dclite.ru/subscribe/" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" onsubmit="yaCounter22051156.reachGoal('get_news');
                            gtag('event', 'get_news', { 'event_category': 'get_news', });">
                        <input type="hidden" name="list_id" value="872040">
                        <input type="hidden" name="no_conf" value="">
                        <input type="hidden" name="notify" value="">
                        <div class="input-group">

                            <input type="email" name="email" id="mce-EMAIL" placeholder="" required="required">
                            <label for="mce-EMAIL">Ваш e-mail</label>
                        </div>
                        <div class="input-group">
                            <button name="subscribe" id="mc-embedded-subscribe" type="submit" class="modal_btn btn-border-style white-style">Получать новости</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="footer__content__block">
                <p class="block-title">Подписывайтесь на наши профили в социальных сетях.</p>
                <p class="social-title">Там мы публикуем новости, полезные материалы, разбираем кейсы и отвечаем на вопросы.</p>
                <ul class="social-list">
                    <li class="social-list__item"><a href="{'insta' | option}" class="social-list__link inst-icon" target="_blank"></a></li>
                    <li class="social-list__item"><a href="{'fb' | option}" class="social-list__link fb-icon" target="_blank"></a></li>
                    <li class="social-list__item"><a href="{'vk' | option}" class="social-list__link vk-icon" target="_blank"></a></li>
                    <li class="social-list__item"><a href="{'tw' | option}" class="social-list__link tw-icon" target="_blank"></a></li>
                    <li class="social-list__item"><a href="{'tube' | option}" class="social-list__link yt-icon" target="_blank"></a></li>
                </ul>
                <a href="tel:{'phone1' | option | preg_replace : '/[^0-9+]/' : ''}" class="footer-phone-link calltracking">{'phone1' | option}</a>
            </div>
        </div>

    </div>
    <div class="footer__copyright">
        <div class="container">
            <div class="footer__copyright__content">
                <p>© {'year' | snippet} {'site_name' | option}. Все права защищены.</p>
                <a href="{40 | url}" class="politic-link">Политика конфиденциальности</a>
            </div>
        </div>
    </div>
</footer>