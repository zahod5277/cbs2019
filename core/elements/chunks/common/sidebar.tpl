<div class="left-side-menu">
    <div class="lside-menu-block">
        <ul class="menu-list">
            {if $_modx->resource.template == 6 || $_modx->resource.template == 7 || $_modx->resource.template == 8 || $_modx->resource.template == 9 || $_modx->resource.template==14}
                <li class="menu-list__item"><a href="{8 | url}" class="menu-list__link center-icon">Что такое центр решений</a></li>
                <li class="menu-list__item"><a href="{9 | url}" class="menu-list__link practice-icon">Практика компании</a></li>
                <li class="menu-list__item"><a href="{15 | url}" class="menu-list__link mainnews-icon">Главные новости</a></li>
                <li class="menu-list__item"><a href="{18220 | url}" class="menu-list__link events-icon">Мероприятия</a></li>
                {else}
                <li class="menu-list__item submenu"> 
                    <a href="javascript:void(0);" class="menu-list__link submenu-link other-serv-link other-icon">Другие услуги</a>
                    <div class="submenu-block">
                        {if $_modx->resource.template != 10 }
                            {set $parent_menu = 23}
                            {set $parent_level = 2}
                            <p class="submenu-title">Услуги</p>
                        {else}
                            {if $_modx->resource.parent == 23 || $_modx->resource.parent == 38}
                                {set $parent_menu = $_modx->resource.id}
                                {set $parent_level = 2}
                            {else}
                                {set $parent_menu = $_modx->resource.parent}
                                {set $parent_level = 2}
                            {/if}
                            <p class="submenu-title">Другие {*$_modx->resource.parent | resource:"menutitle" | ereplace : '/услуг[аиуой]/iu' : '' | lower*} услуги</p>
                        {/if}
                        <a href="javascript:void(0);" class="submenu-close-link"></a>
                        {'pdoMenu' | snippet : [
                            'cache' => 1,
                            'fastMode' => 1,
                            'resources' => '-'~$_modx->config.site_start,
                            'parents' => $parent_menu,
                            'tplOuter' => '@INLINE <ul class="submenu__list">{$wrapper}</ul>',
                            'tplInner' => '@INLINE {$wrapper}',
                            'tplCategoryFolder' => '@FILE:chunks/common/menu/menu.sidebar.services.subcategory.row.tpl',
                            'tpl' => '@INLINE <li class="submenu__list__item">
                                <a href="{$link}" title="{$pagetitle}" class="submenu__list__link {$classnames}">{$menutitle}</a>
                            </li>{$wrapper}',
                            'level' => $parent_level
                        ]}
                    </div>
                </li>
                <li class="menu-list__item"><a href="{7 | url}" class="menu-list__link calc-icon">Калькулятор стоимости</a></li>
                <li class="menu-list__item"><a href="#offer_modal" class="modal-win menu-list__link comm-icon">Коммерческое предложение</a></li>
                <li class="menu-list__item"><a href="#send_to_email_modal" target="_blank" class="modal-win menu-list__link doc-icon">Скачать презентацию</a></li>

                {if $video = $_modx->resource.video}
                    <li class="menu-list__item"><a href="{$video}" title="{$pagetitle}" class="menu-list__link video-icon video-link-modal">Видео об услуге</a></li>
                    {/if}
                <li class="menu-list__item"><a href="#question_modal" class="modal-win menu-list__link question-icon">Задать вопросы</a></li>
                {/if}
        </ul>
    </div>
</div>