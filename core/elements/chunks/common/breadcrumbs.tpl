{'pdoCrumbs' | snippet : [
    'fastMode' => 1,
    'showHome' => 1,
    'tpl' => '@INLINE <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="{$link}" class="breadcrumbs__link" itemprop="item"><span itemprop="name">{$menutitle}</span></a><link itemprop="url" href="{$link}"><meta itemprop="position" content="{$idx}"></li>',
    'tplWrapper' => '@INLINE <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumbs">{$output}</ul>',
    'tplCurrent' => '@INLINE <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><link itemprop="item" href="{$link}"><span itemprop="name" class="current">{$menutitle?:$pagetitle}</span><link itemprop="url" href="{$link}"><meta itemprop="position" content="{$idx}"></li>',
    'tplHome' => '@INLINE <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/" itemprop="item" class="breadcrumbs__link"><span itemprop="name">Главная</span></a><link itemprop="url" href="/"><meta itemprop="position" content="{$idx}"></li>'
]}