<div class="main-section">
    <div class="video-block">
        {var $video = ('video' | option)}
        {if $video}
        <div id="bgndVideo" class="player" data-property="{ videoURL:'http://youtu.be/{$video}',containment:'.main-section',autoplay:true, mute:true, startAt:0, opacity:1 }"></div>
        {/if}
    </div>
    <div class="container">
        <h1 class="h1 main-section__title">{'title_slider' | option}</h1>
        <h2 class="h5 main-section__subtitle">{'subtitle_slider' | option}</h2>
        <div class="main-section__links">
            <div class="links-row">
                 <a href="#client_modal" class="client-btn red-btn-style modal-win">Стать клиентом</a>
                {var $video_present = ('video_present' | option)}
                {if $video_present}
                    <a href="http://www.youtube.com/watch?v={$video_present}" class="video-btn video-link-modal">Видео о компании</a>
                {/if}
            </div>
            {var $pdf_present = ('pdf_present' | option)}
            {if $pdf_present}
                <a href="{$pdf_present}" class="presentation-btn">Презентация компании</a>
            {/if}
        </div>
        <div class="main-section__numbers">
            {('adv_slider' | option) | adv_slider}
        </div>
    </div>
</div>