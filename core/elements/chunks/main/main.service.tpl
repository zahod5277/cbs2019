<div class="main-service">
    <div class="container">
        <div class="main-service__content">
            {if $_modx->resource.services_main}
                {set $services_main = json_decode($_modx->resource.services_main, true)}
                {foreach $services_main as $service_main}
                    {if $service_main@first}
                        <div class="main-service__content__block">
                            <h2 class="block-title">{$service_main.title}</h2>
                            <div class="block-descr">
                                {$service_main.desc}
                            </div>
                        </div>
                        {set $service_items = json_decode($service_main.items, true)}
                        {foreach $service_items as $service_item}
                            <a href="{$service_item.uri | url}">
                                <div class="main-service__content__block">
                                    <p class="block-title">{$service_item.title}</p>
                                    <div class="block-descr">
                                        <p>{$service_item.desc}</p>
                                    </div>
                                    <a href="{$service_item.uri | url}" class="link-style block-link">Перейти</a>
                                </div>
                            </a>
                        {/foreach}
                    {/if}
                {/foreach}
            {/if}
        </div>
    </div>
</div>