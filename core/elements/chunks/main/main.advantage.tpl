<div class="main-advantage">
    <div class="container">
        {if $_modx->resource.advantage_main}
            {set $advantage_main = json_decode($_modx->resource.advantage_main, true)}
            {foreach $advantage_main as $advantage_block}
                {if $advantage_block@first}
                    <h2 class="h3 main-advantage__title">{$advantage_block.title}</h2>
                    <div class="main-advantage__content">
                        {set $advs = json_decode($advantage_block.items, true)}
                        {foreach $advs as $adv_item}
                            <div class="main-advantage__content__block">
                                <div class="icon-block"><img data-src="{$adv_item.img|image_optimize}" alt="{$adv_item.title}" class="lazy icon-image"></div>
                                <div class="descr-block">
                                    <p class="block-title">{$adv_item.title}</p>
                                    <p>{$adv_item.desc}</p>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                {/if}
            {/foreach}
        {/if}
    </div>
</div>