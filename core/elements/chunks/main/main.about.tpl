<div class="main-about">
    <div class="container">
        {if $_modx->resource.about_main}
            {set $about_main = json_decode($_modx->resource.about_main, true)}
            {foreach $about_main as $about}
                {if $about@first}
                    <div class="main-about__content">
                        <div class="main-about__content__left-side">
                            <h2 class="h3 main-about__title">{$about.title}</h2>
                            <p class="main-about__subtitle h6">{$about.subtitle}</p>
                            <div class="descr-block">
                                {$about.desc}
                            </div>
                            <a href="{$about.uri | url}" class="about-link link-style">{$about.link_text}</a>
                        </div>
                        <div class="main-about__content__right-side lazy" data-bg="url({$about.img | phpthumbon:"w=600&h=1000&zc=Tq=80&f=jpg"|image_optimize})"></div>
                    </div>
                {/if}
            {/foreach}
        {/if}
    </div>
</div>