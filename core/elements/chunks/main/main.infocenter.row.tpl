{if $idx==1}
    <div class="tab-info__content__block type-image">
        <a href="{$uri}" title="{$_pls["tv.image"]["altTag"]?:$pagetitle | htmlent}">
            <div class="image-block">
                <img data-src="{$_pls["tv.image"]["sourceImg"]["src"] | phpthumbon:"w=425&h=184&q=70&zc=1"|image_optimize}" alt="{$_pls["tv.image"]["altTag"]?:$pagetitle | htmlent}" class="lazy case-image">
            </div>
            <div class="descr-block">
                <p class="block-title">{$pagetitle}</p>
                {$introtext?:"<p>"~($content | notags | stripmodxtags | truncate : 80 : "..." : true : true)~"</p>"}
            </div>
            <div class="info-block">
                <span class="category">{($parent | resource:"menutitle")?:($parent | resource:"pagetitle")}{if $_pls["tv.tags"]}, 
                    {foreach ($_pls["tv.tags"]|split) as $tag last=$last}
                    {if $last}{$tag}{else}{$tag}, {/if}
                {/foreach}
            {/if}</span>
        <span class="date">{$publishedon | dateago:"{\"dateNow\":0, \"dateFormat\":\"d F Y\", \"dateDay\":\"d F Y\", \"dateHours\":0}"}</span>
    </div>
</a>
</div>
{else}
    <div class="tab-info__content__block">
        <a href="{$uri}" title="{$_pls["tv.image"]["altTag"]?:$pagetitle | htmlent}">
            <div class="descr-block">
                <p class="block-title">{$pagetitle}</p>
                {if $introtext?}
                    <p>{$introtext |notags | stripmodxtags | truncate : 140 : "..." : true}</p>
                {else}
                    <p>{($content | notags | stripmodxtags | truncate : 140 : "..." : true)}</p>
                {/if}
            </div>
            <div class="info-block">
                <span class="category">
                    {($parent | resource:"menutitle")?:($parent | resource:"pagetitle")}
                    {if $_pls["tv.tags"]}, 
                        {foreach ($_pls["tv.tags"]|split) as $tag last=$last}
                            {if $last}{$tag}{else}{$tag}, {/if}
                        {/foreach}
                    {/if}
                </span>
                <span class="date">{$publishedon | dateago:"{\"dateNow\":0, \"dateFormat\":\"d F Y\", \"dateDay\":\"d F Y\", \"dateHours\":0}"}</span>
            </div>
        </a>
    </div>
    {/if}
