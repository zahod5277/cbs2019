<div class="{if $_modx->resource.template==10}service-clients {/if}main-clients" id="our_clients">
    <div class="container">
        <div class="title-row">
            <div class="h3 main-clients__title">Наши клиенты</div>
            <a href="#client_modal" class="client-link btn-border-style modal-win">Стать клиентом</a>
        </div>
        <div class="main-clients__content">
            {if (1 | resource:"clients")}
                {set $clients = json_decode((1 | resource:"clients"), true)}
                {foreach $clients as $client}
                    <div class="main-clients__content__block">
                        <img data-src="{$client.logo | phpthumbon:"q=70"|image_optimize}" alt="{$client.name | htmlent}" class="lazy client-image">
                    </div>
                {/foreach}
                <div class="main-clients__content__block">
                    <span class="client-link" rel="nofolow">
                        <img src="/assets/tpl/images/svg/building-icon.svg" alt="build" class="icon-block">
                        <p>и еще 350 компаний</p>
                    </span>
                </div>
            {/if}
        </div>
    </div>
</div>