
{if $_modx->getPlaceholder("visible_news_rubrics_link")}
    {$_modx->setPlaceholder("visible_news_rubrics_link", $_modx->getPlaceholder("visible_news_rubrics_link") ~ "<a href=\"javascript:void(0);\" data-tab=\"{$idx}\" class=\"tab-link\">"~$pagetitle~"</a>")}
{else}
    {$_modx->setPlaceholder("visible_news_rubrics_link", "<a href=\"javascript:void(0);\" data-tab=\"1\" class=\"tab-link active\">"~$pagetitle~"</a>")}
{/if}
<div class="tab-info" {if $idx == 1}style="display: block"{/if} data-tab="{$idx}">
    <div class="tab-info__content">
        {$_modx->runSnippet('pdoResources', [
            'includeContent' => 1,
            'includeTVs' => 'image,tags,main_news',
            'parents' => $id,
            'templates' => 9,
            'sortby' => '{"main_news": "DESC","publishedon": "DESC"}',
            'showLog' => 0,
            'tpl' => '@FILE:chunks/main/main.infocenter.row.tpl',
            'limit' => 5
        ])}
    </div>
</div>