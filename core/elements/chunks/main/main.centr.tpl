<div class="main-centr">
    <div class="container">
        <div class="title-row">
            <h2 class="h3 main-centr__title">Центр решений</h2>
            <div class="main-centr__info">
                <p class="h6">Еженедельно мы публикуем и рассылаем информацию об основных изменениях в сфере финансов, налогов и учета</p>
            </div>
        </div>
        <div class="main-centr__content">
            <div class="main-centr__content__links">
                {if !$block_news = $_modx->cacheManager->get('block_news')}
                {'pdoResources' | snippet : [
                    'cache' => 1,
                    'fastMode' => 1,
                    'parents' => 8,
                    'tpl' => '@FILE:chunks/main/main.infocenter.tab.tpl',
                    'limit' => 10,
                    'resources' => $_modx->resource.visible_news_rubrics,
                    'tplWrapper' => '@INLINE {$output}',
                    'toPlaceholder' => 'news_block'
                ]}
                {set $block_news = $_modx->getPlaceholder('news_block')}
                {set $null = $_modx->cacheManager->set('block_news', $block_news, 43200)}
                {set $null = $_modx->cacheManager->set('visible_news_rubrics_link', $_modx->getPlaceholder('visible_news_rubrics_link'), 43200)}
                {/if}
                {$_modx->cacheManager->get('visible_news_rubrics_link')}
            </div>
            <div class="main-centr__content__tabs">
                {$block_news}
            </div>
            <div class="btn-row">
                <a href="{9 | url}" class="btn-style centr-btn">Перейти в центр решений</a>
            </div>
        </div>
    </div>
</div>