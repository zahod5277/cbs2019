<div class="main-private">
    <div class="container">
        {if $_modx->resource.private_main}
            {set $private_main = json_decode($_modx->resource.private_main, true)}
            {foreach $private_main as $private_block}
                {if $private_block@first}
                    <div class="main-private__content">
                        <div class="main-private__content__left-side">
                            <p class="subinfo">{$private_block.subtitle}</p>
                            <h2 class="main-private__title">{$private_block.title}</h2>
                            <div class="main-private__info">
                                <p class="h6">{$private_block.desc}</p>
                            </div>
                            <div class="main-private__link">
                                <a href="{$private_block.uri | url}" class="main-private-link link-style" onclick="yaCounter22051156.reachGoal('click_private'); gtag('event', ‘click_private’, { 'event_category' : 'click_private', });">{$private_block.link_text}</a>
                            </div>
                        </div>
                        <div class="lazy main-private__content__right-side" data-bg="url('{$private_block.img | phpthumbon:"w=1000&q=90&f=jpg"|image_optimize}')">
                        </div>
                    </div>
                {/if}
            {/foreach}
        {/if}
    </div>
</div>