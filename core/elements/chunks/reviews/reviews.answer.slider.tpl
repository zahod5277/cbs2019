<div class="answer-slider" id="answer-slider">
    <div class="container">
        <div class="title-row">
            <div class="h3 answer-slider__title">Отзывы клиентов</div>
            <div class="answer-slider__btns">
                <div class="answers-type">
                    <a href="javascript:void(0);" data-type="all" class="answer-type-link active">Все</a>
                    <a href="javascript:void(0);" data-type="info" class="answer-type-link">Текстовые</a>
                    <a href="javascript:void(0);" data-type="video" class="answer-type-link">Видео</a>
                </div>
                <div class="answers-count">
                    <div class="slide-val">
                        <span class="current-slide"></span>
                        <span class="all-slides"></span>
                    </div>
                    <div class="slide-btns"></div>
                </div>
            </div>
        </div>
        {if $_modx->resource.id == 25 || $_modx->resource.parent == 25}
            {include 'file:chunks/reviews/reviews.audit.review.tpl'}
        {else}
            {include 'file:chunks/reviews/reviews.review.tpl'}
        {/if}
        <div class="btn-row">
            <a href="#answer_modal" class="btn-border-style answer-btn modal-win">Оставить отзыв</a>
        </div>
    </div>
</div>