{if $answer_slider = (4 | resource:"answer_slider")}
    <div class="answer-slider__content">
        <div id="answers-slider" class="owl-carousel answers-slider">
            {foreach json_decode($answer_slider, true) as $answer_item}
                {if $answer_item.MIGX_type == 'video'}
                    <div class="slide-item type-video" data-type="video">
                        <div class="slide-item__left-side">
                            <div class="block-image">
                                <img data-src="{$answer_item.logo|image_optimize}" alt="{$answer_item.name | htmlent}" class="lazy company-image">
                            </div>
                            <div class="block-descr">
                                <p class="answer-title">{$answer_item.name}</p>
                                <p>{$answer_item.rang}</p>
                                <p>{$answer_item.fio}</p>
                            </div>
                        </div>
                        <div class="slide-item__right-side">
                            <a href="{$answer_item.link}" rel="nofollow" title="Видеоотзыв компании {$answer_item.name | htmlent}" class="video-link video-link-modal">
                                <img data-src="{$answer_item.img | phpthumbon:"w=880&h=432&q=50&zc=T&bg=ffffff"|image_optimize}" alt="video" class="lazy video-preview">
                            </a>
                        </div>
                    </div>
                {else}
                    <div class="slide-item type-info" data-type="info">
                        <div class="slide-item__left-side">
                            <div class="block-image">
                                <img data-src="{$answer_item.logo|image_optimize}" alt="{$answer_item.name | htmlent}" class="lazy company-image">
                            </div>
                            <div class="block-descr">
                                <p class="answer-title">{$answer_item.name}</p>
                                <p>{$answer_item.rang}</p>
                                <p>{$answer_item.fio}</p>
                            </div>
                        </div>
                        <div class="slide-item__right-side">
                            <div class="slide-descr">
                                <div class="slide-descr__left-side">
                                    {$answer_item.intro}
                                    <div class="btn-row">
                                        <!-- <a href="#full{$answer_item@index}" rel="nofollow" title="Отзыв компании {$answer_item.name | htmlent}" class="read-more-link">Читать полностью...</a> -->
                                        <div id="full{$answer_item@index}" class="zoom-anim-dialog mfp-hide modal-form-style">
                                            <div class="modal-body">
                                                <div class="modal-title">Отзыв компании {$answer_item.name}</div>
                                                <div class="modal-subtitle">{$answer_item.full}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide-descr__right-side">
                                    <a href="{$answer_item.img}" title="Отзыв компании {$answer_item.name | htmlent}" class="doc-link">
                                        <img data-src="{$answer_item.img | phpthumbon:"w=228&h=320&q=50&zc=T&bg=ffffff"|image_optimize}" alt="Отзыв компании {$answer_item.name | htmlent}" class="lazy doc-image">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                {/if}
            {/foreach}
        </div>
    </div>
{/if}