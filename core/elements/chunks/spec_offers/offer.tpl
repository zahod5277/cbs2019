<div class="left-side-type">
    {include 'file:chunks/common/sidebar.tpl'}
    <div class="left-side-content">
        <div class="service-features">
            <div class="container">
                <div class="service-features__content">
                    <div class="service-features__content__left-side service-features__content__left-side--no-margin-top">
                        <h3 class="h3 service-features__title">{$_modx->resource.pagetitle}</h3>
                        <p class="h6 service-features__subtitle">{$_modx->resource.introtext}</p>
                        <div class="service-features__descr list-benefits-text">
                            {$_modx->resource.content}
                        </div>
                    </div>
                    <div class="service-how__content__right-side">
                        <div class="spec-offer">
                            <div class="h3 service-callback__title">Ваша заявка на участие в акции</div>
                            <p class="spec-offer__form-title">Оставьте свой номер и наш менеджер свяжется с вами в течение 5
                                минут.</p>
                            {'!AjaxForm' | snippet : [
                                'frontend_css' => 0,
                                'snippet' => 'FormIt',
                                'hooks' => 'spam,csrf,modBitrixLead,FormItSaveForm',
                                'form' => 'offer_form_tpl',
                                'formSelector' => 'offer_form',
                                'emailTo' => $_modx->config['e-mail'],
                                'emailFrom' => "noreply@{$_modx->config.http_host}",
                                'emailSubject' => 'Обратный звонок '~$_modx->config.http_host,
                                'validate' => 'phone_f:required,email:blank',
                                'emailTpl' => 'message-discounts',
                                'btxFieldnames' => '{"phone_f":"PHONE","name_f":"NAME", "title": "TITLE", "roistat": "UF_CRM_1565004282"}',
                                'formFields' => 'phone_f,name_f,email',
                                'fieldNames' => 'phone_f==Телефон,name_f==ФИО,email==SPAM',
                                'formName' => 'Форма Акция',
                                'validationErrorMessage' => 'Вы не указали свой телефон',
                                'successMessage' => 'Мы получили вашу заявку и свяжемся с вами'
                            ]}
                        </div>
                        {if $_modx->resource.workperson?}
                            {var $person = $_modx->resource.workperson|fromJSON}
                            <div class="how-image-block">
                                <img data-src="{$person[0]['photo']}"
                                     class="how-work-image lazy">
                            </div>
                            <div class="how-block">
                                <p class="person-name">{$person[0]['fio']}</p>
                                <p class="person-who">{$person[0]['rang']}</p>
                                <div class="person-data">
                                    <a href="tel:{$person[0]['phone']?:('phone1' | option)}"
                                       class="phone-link">{$person[0]['phone']?:('phone1' | option)}</a>
                                    <a href="mailto:{$person[0]['email']?:('email' | option)}"
                                       class="mail-link">{$person[0]['email']?:('email' | option)}</a>
                                </div>
                            </div>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
        {include 'file:chunks/services/services.ratings.tpl'}
    </div>
</div>
