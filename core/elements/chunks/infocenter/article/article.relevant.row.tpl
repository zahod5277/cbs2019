<div class="case-block {if $_pls["tv.image"]["sourceImg"]["src"]}photo-block m2{else}text-block m1{/if}">
    <a href="{$uri}" title="{$pagetitle | htmlent}" class="article-link">
        {if $_pls["tv.image"]["sourceImg"]["src"]}
            <div class="image-block">
                <div class="block-background lazy" data-bg="url('{$_pls["tv.image"]["sourceImg"]["src"] | phpthumbon:"w=422&h=183&q=70&zc=C"|image_optimize}')"></div>
            </div>
        {/if}
        <div class="descr-block">
            <p class="block-title">{$pagetitle| truncate : 60: "..." :true}</p>
            {if $introtext?}
                <p>{$introtext|notags | stripmodxtags | truncate : 140 : "..." : true}</p>
            {else}
                <p>{($content | notags | stripmodxtags | truncate : 140 : "..." : true)}</p>
            {/if}
        </div>
        <div class="info-block m1">
            <span class="category">{($parent | resource:"menutitle")?:($parent | resource:"pagetitle")}{if $_pls["tv.tags"]}, 
                {foreach ($_pls["tv.tags"]|split) as $tag last=$last}
                {if $last}{$tag}{else}{$tag}, {/if}
            {/foreach}
            {/if}</span>
            <span class="date">{$publishedon | dateago:"{\"dateNow\":0, \"dateFormat\":\"d F Y\", \"dateDay\":\"d F Y\", \"dateHours\":0}"}</span>
        </div>
    </a>
</div>