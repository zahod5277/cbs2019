<div class="cases-slider">
    <div class="container">
        <div class="title-row">
            <h2 class="h3 cases-slider__title">Рекомендуем почитать</h2>
            <div class="cases-slider__btns">
                <div class="answers-count">
                    <div class="slide-val">
                        <span class="current-slide"></span>
                        <span class="all-slides"></span>
                    </div>
                    <div class="slide-btns"></div>
                </div>
            </div>
        </div>
        <div class="service-cases__content">
            <div id="know-slider" class="owl-carousel servicecases-slider">{$output}</div>
        </div>
    </div>
</div>