<table width="100%" cellpadding="0" cellspacing="0" border="0" data-mobile="true" dir="ltr" align="center" data-width="600" style="background-color: rgb(255, 255, 255);">
    <tbody>
    <tr>
        <td align="center" valign="top" style="padding:0;margin:0;" class="responsive-cell">

            <table align="center" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" width="600" class="wrapper" style="width: 600px;">
                <tbody>

                <tr>
                    <td align="center" valign="top" style="margin: 0px; padding: 0px;" class="responsive-cell">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td valign="top" align="left" width="50%" style="padding: 0px; margin: 0px;" class="responsive-cell">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="margin: 0px; padding: 0px;" align="center" valign="top" class="responsive-cell">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text" class="text-block empty-class">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" align="left" class="lh-1" style="padding: 10px; margin: 0px; font-size: 16px; font-family: 'Times New Roman', Times, serif; line-height: 1.15;">
                                                            <table border="0" cellpadding="0" cellspacing="0" align="left" data-editable="image" style="margin: 0px; padding: 0px;mso-table-rspace: 7pt" data-mobile-stretch="0" id="edienjy8" width="160">
                                                                <tbody>
                                                                <tr>
                                                                    <td valign="top" align="left" style="display: inline-block; padding: 0px 10px 10px 0px; margin: 0px;" class="tdBlock responsive-cell">
                                                                        <img src="https://cbscg.ru/assets/Centrresheniy_images/main-logo[1].png" width="144" data-src="https://cbscg.ru/assets/Centrresheniy_images/main-logo[1].png" data-origsrc="https://cbscg.ru/assets/Centrresheniy_images/main-logo[1].png" height="82" class="mobile-image-stretch-disabled" style="border-width: 0px; border-style: none; border-color: transparent; font-size: 12px; display: block;">
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table><span style="font-family:Arial,Helvetica,sans-serif;color:#262626;font-size:14px">Аудит</span>
                                                            <div><span style="font-family:Arial,Helvetica,sans-serif;color:#262626;font-size:14px">Учёт</span></div>
                                                            <div><span style="font-family:Arial,Helvetica,sans-serif;color:#262626;font-size:14px">Налоги</span></div>
                                                            <div><span style="font-family:Arial,Helvetica,sans-serif;color:#262626;font-size:14px">Консалтинг</span></div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td valign="top" align="left" width="50%" style="padding: 0px; margin: 0px;" class="responsive-cell">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="margin: 0px; padding: 0px;" align="center" valign="top" class="responsive-cell">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text" class="text-block empty-class">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" align="left" class="lh-1" style="padding: 10px; margin: 0px; font-size: 16px; font-family: Verdana, sans-serif; line-height: 1.15;">
                                                            <div style="text-align: right;">
                                                                <a href="tel:+74956362769" target="_blank" title="+7 (495) 636-27-69" style="background-color: transparent;">
                                                                    +7 (495) 636-27-69
                                                                </a>
                                                            </div>
                                                            <div style="text-align: right; ">
                                                                <a href="tel:+79778933769" title="+7 (977) 893-37-69" target="_blank">
                                                                    +7 (977) 893-37-69
                                                                </a>
                                                                <br>
                                                            </div>
                                                            <div style="text-align: right; ">
                                                                <a href="mailto:info@cbscg.ru" target="_blank" title="info@cbscg.ru">
                                                                    info@cbscg.ru
                                                                </a>
                                                                <br>
                                                            </div>
                                                            <div style="text-align: right; ">
                                                                <a href="https://cbscg.ru" title="www.cbscg.ru" target="_blank">
                                                                    www.cbscg.ru
                                                                </a>
                                                                <br>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="margin: 0px; padding: 0px;" class="responsive-cell">
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" data-editable="line">
                            <tbody>
                            <tr>
                                <td valign="top" align="center" style="padding: 10px 0px; margin: 0px;">
                                    <div style="height:1px;line-height:1px;border-top-width:3px;border-top-style:solid;border-top-color:#dedede;"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="" width="1" height="1" style="display:block;" createnew="true"></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="margin:0;padding:0;" class="responsive-cell">

                        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text" class="text-block">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" class="lh-1" style="padding: 0px; margin: 0px; font-family: Arial, Helvetica, sans-serif; color: rgb(38, 38, 38); font-size: 16px; line-height: 1.15;">
                                    <div><span style="font-family:Arial,Helvetica,sans-serif;color:#262626;font-size:18px;font-weight:bold;"><br></span></div>
                                    <span style="font-family:Arial,Helvetica,sans-serif;color:#262626;font-size:18px;font-weight:bold;">
                                        [[+title]]
                                    </span>
                                    <div><span style="color: rgb(38, 38, 38); font-size: 18px;"><br></span></div>
                                    <div>
                                        <span style="color: rgb(38, 38, 38); font-size: 18px;"><br>
                                        <span style="font-size: 14px;">
                                            <span style="font-family: Verdana, sans-serif;">
                                                [[+content]]
                                            </span>
                                        </span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="margin: 0px; padding: 0px;" class="responsive-cell">
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" data-editable="line">
                            <tbody>
                            <tr>
                                <td valign="top" align="center" style="padding: 10px 0px; margin: 0px;">
                                    <div style="height:1px;line-height:1px;border-top-width:3px;border-top-style:solid;border-top-color:#dedede;"><img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="" width="1" height="1" style="display:block;" createnew="true"></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="margin: 0px; padding: 0px;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td valign="top" align="left" width="5%" style="padding: 0px; margin: 0px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="margin: 0px; padding: 0px;" align="center" valign="top" class="responsive-cell">
                                                <table cellpadding="0" cellspacing="0" align="center" data-editable="image" data-mobile-stretch="0" width="40">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" align="center" style="display: inline-block; padding: 10px 0px; margin: 0px;" class="tdBlock responsive-cell">
                                                            <a href="https://www.instagram.com/cbsgroupnews/">
                                                                <img src="https://cbscg.ru/assets/tpl/inst1.png" width="40" style="border-width: 0px; border-style: none; border-color: transparent; font-size: 12px; display: block;" data-src="https://cbscg.ru/assets/tpl/insta1.png" data-origsrc="https://cbscg.ru/assets/tpl/insta1.png" height="40" class="mobile-image-stretch-disabled">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td valign="top" align="left" width="5%" style="padding: 0px; margin: 0px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="margin: 0px; padding: 0px;" align="center" valign="top" class="responsive-cell">
                                                <table cellpadding="0" cellspacing="0" align="center" data-editable="image" data-mobile-stretch="0" width="40">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" align="center" style="display: inline-block; padding: 10px 0px; margin: 0px;" class="tdBlock responsive-cell">
                                                            <a href="https://www.facebook.com/cbsgroupnews/">
                                                                <img src="https://cbscg.ru/assets/tpl/fb1.png" width="40" style="border-width: 0px; border-style: none; border-color: transparent; font-size: 12px; display: block;" data-src="https://cbscg.ru/assets/tpl/fb1.png" data-origsrc="https://cbscg.ru/assets/tpl/fb1.png" height="40" class="mobile-image-stretch-disabled"></td>
                                                            </a>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td valign="top" align="left" width="5%" style="padding: 0px; margin: 0px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="margin: 0px; padding: 0px;" align="center" valign="top" class="responsive-cell">
                                                <table cellpadding="0" cellspacing="0" align="center" data-editable="image" data-mobile-stretch="0" width="40">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" align="center" style="display: inline-block; padding: 10px 0px; margin: 0px;" class="tdBlock responsive-cell">
                                                            <a href="https://vk.com/public178536434">
                                                                <img src="https://cbscg.ru/assets/tpl/vk1.png" width="40" style="border-width: 0px; border-style: none; border-color: transparent; font-size: 12px; display: block;" data-src="https://cbscg.ru/assets/tpl/vk1.png" data-origsrc="https://cbscg.ru/assets/tpl/vk1.png" height="40" class="mobile-image-stretch-disabled">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td valign="top" align="left" width="5%" style="padding: 0px; margin: 0px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="margin: 0px; padding: 0px;" align="center" valign="top" class="responsive-cell">
                                                <table cellpadding="0" cellspacing="0" align="center" data-editable="image" data-mobile-stretch="0" width="40">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" align="center" style="display: inline-block; padding: 10px 0px; margin: 0px;" class="tdBlock responsive-cell">
                                                            <a href="https://www.youtube.com/channel/UCiz33DwjCuBTzfrGSTaEHzQ">
                                                                <img src="https://cbscg.ru/assets/tpl/yt1.png" width="40" style="border-width: 0px; border-style: none; border-color: transparent; font-size: 12px; display: block;" data-src="https://cbscg.ru/assets/tpl/yt1.png" data-origsrc="https://cbscg.ru/assets/tpl/yt1.png" height="40" class="mobile-image-stretch-disabled">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td valign="middle" align="left" width="25%" style="padding: 0px; margin: 0px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="margin: 0px; padding: 0px;" align="center" valign="middle" class="responsive-cell">
                                                <table cellpadding="0" cellspacing="0" align="center" data-editable="image" data-mobile-stretch="0" width="40">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="middle" align="center" style="display: inline-block; font-weight: bold;padding: 10px 0px; margin: 0px;" class="tdBlock responsive-cell">
                                                            #cbsgroup
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td valign="top" align="left" width="21%" style="padding: 0px; margin: 0px;"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>