<div class="article-discounts">
    <div class="container">
        <div class="article-discounts__content">
            <div class="article-discounts__content__left-side">
                <div class="h3 article-discounts__title">10% скидка при первом<br> обращении</div>
                <p class="form-title">Воспользуйтесь нашим скидочным предложением. Мы даем настоящие а не виртуальные скидки. Вы убедитесь в этом лично, заполнив форму заявки. </p>
                {'!AjaxForm' | snippet : [
                    'frontend_css' => 0,
                    'snippet' => 'FormIt',
                    'hooks' => 'spam,csrf,modBitrixLead,FormItSaveForm',
                    'form' => 'discounts_form_tpl',
                    'formSelector' => 'discounts_form',
                    'emailTo' => $_modx->config['e-mail'],
                    'emailFrom' => "noreply@"~$_modx->config.http_host,
                    'validate' => 'phone_f:required,email:blank',
                    'emailTpl' => 'message-discounts',
                    'btxFieldnames' => '{"phone_f":"PHONE","name_f":"NAME", "title": "TITLE","roistat": "UF_CRM_1565004282"}',
                    'emailSubject' => 'Заявка с сайта '~$_modx->config.http_host,
                    'clearFieldsOnSuccess' => true,
                    'emailUseFieldForSubject' => 1,
                    'formFields' => 'phone_f,name_f',
                    'fieldNames' => 'phone_f==Телефон,name_f==Имя',
                    'formName' => '10% скидки при первом обращении',
                    'validationErrorMessage' => 'Вы не указали свой телефон',
                    'successMessage' => 'Заявка отправлена, мы с вами свяжемся'
                ]}
            </div>
            <div class="lazy article-discounts__content__right-side" data-bg="url({'/assets/tpl/images/gift-person-image.jpg'|image_optimize})">
            </div>
        </div>
    </div>
</div>