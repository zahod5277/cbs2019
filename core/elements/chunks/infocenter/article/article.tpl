<div class="left-side-type">
    {include 'file:chunks/common/sidebar.tpl'}
    <div class="left-side-content">
        <div class="article-main">
            <div class="article-main__breadcrumbs-block">
                <div class="container">
                    {include 'file:chunks/common/breadcrumbs.tpl'}
                </div>
            </div>
            <div class="container">
                <div class="article-main__content">
                    <div class="article-main__content__left-side">
                        <div id="mainContent" class="main-content" itemscope="" itemtype="http://schema.org/Article">
                            <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                                <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                                    <img itemprop="url image" src="logo_cbs.png" style="display:none;"/>
                                    <meta itemprop="width" content="64">
                                    <meta itemprop="height" content="64">
                                </div>
                                <meta itemprop="name" content="https://cbscg.ru/">
                                <meta itemprop="telephone" content="{'cbs.phone1'|config}">
                                <meta itemprop="address" content="Московская область, г. Мытищи,
                    ул. Карла Маркса д. 2">
                            </div>
                            <meta itemprop="dateModified" content="{$_modx->resource.publishedon}"/>
                            <link itemprop="mainEntityOfPage" href="[[~[[*id]]]]"/>
                            <div class="main-content__top">
                                <div class="tags-container">
                                    <div class="tag-block">{($_modx->resource.parent | resource:"menutitle")?:($_modx->resource.parent | resource:"pagetitle")}</div>
                                    {if $_modx->resource.tags}
                                        {var $tags = $_modx->resource.tags|split}
                                        {foreach $tags as $tag}
                                            {if $tag@first}{var $tag_relevant = $tag}{/if}
                                            <div class="tag-block">{$tag}</div>
                                        {/foreach}
                                    {/if}
                                </div>
                                <div itemscope itemprop="image" itemtype="http://schema.org/ImageObject">
                                    <img itemprop="url contentUrl" src="logo_cbs.png" style="display:none;">
                                    <meta itemprop="url" content="/logo_cbs.png">
                                    <meta itemprop="width" content="968">
                                    <meta itemprop="height" content="968">
                                </div>
                                <div class="time-block">
                                    <time itemprop="datePublished"
                                          datetime="{$_modx->resource.publishedon}">{var $date_publ = $_modx->resource.publishedon | dateago:'{ "dateNow":0, "dateFormat":"d F Y"}'}{$date_publ}</time>
                                </div>
                            </div>
                            <h1 class="article-main__title" itemprop="headline">{$_modx->resource.pagetitle}</h1>
                            <h2 class="h6">{$_modx->resource.h2}</h2>
                            <div class="main-content__info">
                                {if $_modx->resource.video}
                                    <iframe style="width:100%;max-width:100%;height:400px"
                                            src="//www.youtube-nocookie.com/embed/{$_modx->resource.video | geturivideo}"
                                            frameborder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                    {if $_modx->resource.subscribeForm == 1}
                                        {'!AjaxForm' | snippet : [
                                        'frontend_css' => 0,
                                        'snippet' => 'FormIt',
                                        'hooks' => 'csrf,email,FormItSaveForm',
                                        'form' => 'article_webinar_form',
                                        'emailTo' => ('manager_mail'|option),
                                        'emailFrom' => "noreply@{$_modx->config.http_host}",
                                        'validate' => 'email:required',
                                        'emailTpl' => 'webinarMail',
                                        'emailSubject' => 'Подписка на вебинар',
                                        'clearFieldsOnSuccess' => true,
                                        'formName' => 'Вебинар',
                                        'validationErrorMessage' => 'Вы не указали свой e-mail адрес',
                                        'successMessage' => '',
                                        ]}
                                    {/if}
                                {/if}
                                <article itemprop="articleBody">{$_modx->resource.content}</article>
                                <div style="display:none;">Автор:<span itemprop="author">CBS group</span></div>
                            </div>
                            <div class="main-content__bottom">
                                <div class="ya-share2"
                                     data-services="vkontakte,facebook,twitter,linkedin,whatsapp,telegram"
                                     data-limit="3"></div>
                                <div class="btns-row">
                                    <a href="javascript:window.print(); void 0;" class="print-btn btn-border-style">Распечатать</a>
                                    <a href="#email_modal" class="mail-btn modal-win btn-border-style">Отправить на
                                        e-mail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {include 'file:chunks/infocenter/article/article.subscribe.events.tpl'}
                </div>
            </div>
        </div>
        {var $relevant = 'getRelevant' | snippet : ['tags' => $tags]}
        {'pdoResources' | snippet : [
        'fastMode' => 1,
        'parents' => 8,
        'tpl' => '@FILE:chunks/infocenter/article/article.relevant.row.tpl',
        'includeTVs' => 'image,tags',
        'includeContent' => 1,
        'limit' => 12,
        'resources' => $relevant,
        'tplWrapper' => '@FILE:chunks/infocenter/article/article.relevant.outer.tpl'
        ]}
        {switch $_modx->resource.parent}
        {case 121,122,124,125,123}
            {include 'file:chunks/infocenter/article/article.discount.tpl'}
        {case default}
            {include 'file:chunks/infocenter/article/article.discount.news.tpl'}
        {/switch}
    </div>
</div>

<div class="subscribe-modal" data-modal="subscribe">
    <i class="subscribe-modal__close" data-modal-close="close">x</i>
    <p class="subscribe-modal__title">{'subscribe-modal-title'|option}</p>
    <p class="subscribe-modal__text">
        {'subscribe-modal-text'|option}
    </p>
    <form action="https://cabinet.dclite.ru/subscribe/" method="post" name="mc-embedded-subscribe-form" class="validate"
          target="_blank"
          onsubmit="yaCounter22051156.reachGoal('get_news');gtag('event', 'get_news', { 'event_category': 'get_news', });">
        <input type="hidden" name="list_id" value="872040">
        <input type="hidden" name="no_conf" value="">
        <input type="hidden" name="notify" value="">
        <div class="subscribe-modal__input-group">
            <input type="email" class="subscribe-modal__input" name="email" id="mce-EMAIL" placeholder="" required="required">
            <label for="mce-EMAIL">Ваш e-mail</label>
        </div>
        <div class="input-group">
            <button id="mc-embedded-subscribe" type="submit" class="btn-style subscribe-modal__button">Получать новости</button>
        </div>
    </form>
</div>