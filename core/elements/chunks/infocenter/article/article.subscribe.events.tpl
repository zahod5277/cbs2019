<div class="article-main__content__right-side">
    <div class="events events--sidebar">
    <div class="article-social-block">
        <p class="block-title">Подписывайтесь на наши профили в соцсетях</p>
        <ul class="social-list">
            <li class="social-list__item"><a href="{'insta' | option}" target="_blank" class="inst-link social-list__link">Репорты в Instagram</a></li>
            <li class="social-list__item"><a href="{'fb' | option}" target="_blank" class="fb-link social-list__link">Серьезный разговор
                    <br>в Facebook</a></li>
            <li class="social-list__item"><a href="{'vk' | option}" target="_blank" class="vk-link social-list__link">Дайджест в Вконтакте</a></li>
            <li class="social-list__item"><a href="{'tw' | option}" target="_blank" class="tw-link social-list__link">Краткие новости в Twitter</a></li>
            <li class="social-list__item"><a href="{'tube' | option}" target="_blank" class="yt-link social-list__link">Полезные видео
                    <br>на канале YouTube</a></li>
        </ul>
    </div>
    {$_modx->runSnippet('@FILE:snippets/getEvents.php',[
        'tpl' => '@FILE:chunks/infocenter/article/article.event.row.tpl'
    ])}
    </div>
    <div class="lazy article-news-mailing" id="mc_embed_signup" data-bg="url('assets/tpl/images/article-right-case-image80.png')">
        <p class="block-title">Подписывайтесь на нашу новостную рассылку</p>
        <p class="block-subtitle">Самые важные новости. Еженедельно. <br>Полностью бесплатно.<br> Рассылка существует с 2011 года.</p>
        <form action="https://cabinet.dclite.ru/subscribe/" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" onsubmit="yaCounter22051156.reachGoal('get_news');
                                gtag('event', 'get_news', { 'event_category': 'get_news', });">
            <input type="hidden" name="list_id" value="872040">
            <input type="hidden" name="no_conf" value="">
            <input type="hidden" name="notify" value="">
            <div class="input-group">
                <input type="email" name="email" id="mce-EMAIL" placeholder="" required="required">
                <label for="mce-EMAIL">Ваш e-mail</label>
            </div>
            <div class="input-group">
                <button id="mc-embedded-subscribe" type="submit" class="btn-border-style">Получать новости</button>
            </div>
        </form>
    </div>
</div>