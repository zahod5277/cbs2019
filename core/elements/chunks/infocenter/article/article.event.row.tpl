<div class="lazy events__item events__item--sidebar case-[[+idx]]" data-bg="url({$img|image_optimize})">
    <div class="events__info">
        <span class="events__item-type events__item-type--[[+color]]">[[+type]]</span>
        <span class="events__item--date">[[+date]]</span>
    </div>
    <div class="events__descr">
        <p class="events__item--title">[[+title]]</p>
        <p class="events__item--subtitle">[[+desc]]</p>
    </div>
    <a href="[[+link]]" class="events__item-link [[+none]]">Подробнее</a>
</div>