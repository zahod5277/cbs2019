{if $_pls['tv.video']?}

    {if $_pls['tv.image']['sourceImg']['src']}
        {var $image = $_pls['tv.image']['sourceImg']['src'] | phpthumbon:"w=424&h=416&q=70&zc=1"|image_optimize}
    {else}
        {var $image = 'assets/tpl/images/info-center-practice-case-full-image.jpg'|image_optimize}
    {/if}

    <div class="lazy practice-main__content__block full-block" data-bg="url('{$image}')">
        <a href="{$uri}" class="video-link">
            <div class="descr-block">
                <p class="block-title">{$pagetitle}</p>
                {if $introtext?}
                <p>{$introtext|notags | stripmodxtags | truncate : 140 : "..." : true}</p>
            {else}
                <p>{($content | notags | stripmodxtags | truncate : 140 : "..." : true)}</p>
            {/if}
            </div>
            <div class="info-block">
                <span class="category">
                    {($parent | resource:'menutitle')?:($parent | resource:'pagetitle')}
                    {if $_pls['tv.tags']}, 
                        {foreach ($_pls['tv.tags']|split) as $tag last=$last}
                            {if $last}
                                {$tag}
                            {else}
                                {$tag}, 
                            {/if}
                        {/foreach}
                    {/if}
                </span>
            </div>
        </a>
    </div>
{else}
    <div class="practice-main__content__block {if $_pls['tv.image']['sourceImg']['src']}photo-block{else}text-block{/if}">
        <a href="{$uri}" title="{$pagetitle | htmlent}" class="article-link">
            {if $_pls['tv.image']['sourceImg']['src']}
                <div class="image-block">
                    {if $_pls["tv.image"]}
                    <div class="lazy block-background" data-bg="url('{$_pls["tv.image"]["sourceImg"]["src"] | phpthumbon:"w=422&h=183&q=70&zc=C"|image_optimize}')">
                    </div>
                    {/if}
                </div>
            {/if}
            <div class="descr-block">
                <p class="block-title">{$pagetitle}</p>
                {if $introtext?}
                    <p>{$introtext|notags | stripmodxtags | truncate : 180 : "..." : true}</p>
                {else}
                    <p>{($content | notags | stripmodxtags | truncate : 180 : "..." : true)}</p>
                {/if}
            </div>
            <div class="info-block">
                <span class="category">
                    {($parent | resource:"menutitle")?:($parent | resource:"pagetitle")}
                    {if $_pls['tv.tags']}, 
                        {foreach ($_pls['tv.tags']|split) as $tag last=$last}
                            {if $last}
                                {$tag}
                            {else}
                                {$tag}, 
                            {/if}
                        {/foreach}
                    {/if}</span>
                <span class="date">{$publishedon | dateago:"{\"dateNow\":0, \"dateFormat\":\"d F Y\", \"dateDay\":\"d F Y\", \"dateHours\":0}"}</span>
            </div>
        </a>
    </div>
{/if}