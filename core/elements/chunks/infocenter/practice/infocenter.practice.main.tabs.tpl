<div class="practice-main__tabs">
    {if $_modx->resource.id == 9 || $_modx->resource.id == 15}
        <span class="tab-link active">Все</span>
    {else}
        <a href="{if $_modx->resource.template == 7}{9 | url}{else}{15 | url}{/if}" title="Все" class="tab-link">Все</a>
    {/if}
    {if $_modx->resource.template == 7}
    {'pdoMenu' | snippet : [
        'cache' => 0,
    	'fastMode' => 1,
    	'parents' => 9,
    	'templates' => $_modx->resource.template,
    	'level' => 1,
    	'tplOuter' => '@INLINE {$wrapper}',
    	'tpl' => '@INLINE <a href="{$link}" title="{$menutitle}" class="tab-link {$classnames}" {$attributes}>{$menutitle}</a>',
    	'tplHere' => '@INLINE <span class="tab-link {$classnames}">{$menutitle}</span>'
        ]}
    {else}
    {'pdoMenu' | snippet : [
        'cache' => 0,
    	'fastMode' => 1,
    	'parents' => 15,
    	'templates' => $_modx->resource.template,
    	'level' => 1,
    	'tplOuter' => '@INLINE {$wrapper}',
    	'tpl' => '@INLINE <a href="{$link}" title="{$menutitle}" class="tab-link {$classnames}">{$menutitle}</a>',
    	'tplHere' => '@INLINE <span class="tab-link {$classnames}">{$menutitle}</span>'
        ]}
    {/if}
</div>