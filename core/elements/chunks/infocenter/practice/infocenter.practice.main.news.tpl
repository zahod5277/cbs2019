{if $_modx->resource.id == 9 || $_modx->resource.id == 15}
    {var $where = ['main_news_gl' => 1]}
{else}
    {var $where = ['main_news' => 1]}
{/if}
{$_modx->runSnippet('!pdoResources', [
    'cache' => 0,
    'resources' => '-121,-122,-124,-125,-123,-18330',
    'includeContent' => 1,
    'includeTVs' => 'image,tags,video,main_news,main_news_gl',
    'parents' => $_modx->resource.id,
    'tpl' => '@FILE:chunks/infocenter/practice/infocenter.practice.main.news.row.tpl',
    'limit' => 1,
    'where' => $where
])}
