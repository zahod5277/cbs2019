{if $_pls['tv.image']['sourceImg']['src']}
    {var $style = 'data-bg="url('~$_pls['tv.image']['sourceImg']['src'] | phpthumbon:"w=850&h=416&q=70&zc=1"|image_optimize~')"'}
{/if}
<div class="lazy practice-main__content__block video-block practice-main__content__block-main" {$style}>
    <a href="{$uri}" class="article-link {if $_pls['tv.video']}video-link{/if}">
        <div class="descr-block">
            <p class="block-title">{$pagetitle}</p>
             {if $introtext?}
                <p class="block-text-description">{$introtext|notags | stripmodxtags | truncate : 140 : "..." : true}</p>
            {else}
                <p class="block-text-description">{($content | notags | stripmodxtags | truncate : 140 : "..." : true)}</p>
            {/if}
        </div>
        <div class="info-block">
            <span class="category">
                {($parent | resource:'menutitle')?:($parent | resource:'pagetitle')}
                    {if $_pls['tv.tags']}, 
                    {foreach ($_pls['tv.tags']|split) as $tag last=$last}
                    {if $last}{$tag}{else}{$tag}, {/if}
                {/foreach}
            {/if}</span>
            <span class="date">{$publishedon | dateago:"{\"dateNow\":0, \"dateFormat\":\"d F Y\", \"dateDay\":\"d F Y\", \"dateHours\":0}"}</span>
        </div>
    </a>
</div>