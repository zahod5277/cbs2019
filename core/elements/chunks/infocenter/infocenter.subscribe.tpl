<div class="infocenter-callback">
    <div class="container">
        <div class="infocenter-callback__content">
            <div class="infocenter-callback__content__left-side" id="mc_embed_signup">
                <div class="h4 block-title">Подписывайтесь на нашу новостную рассылку</div>
                <p class="form-title">Сэкономьте 4 часа в неделю. Выходит еженедельно. Полностью бесплатно.</p>
                <form action="https://cabinet.dclite.ru/subscribe/" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" onsubmit="yaCounter22051156.reachGoal('get_news');gtag('event', 'get_news', { 'event_category': 'get_news', });">
                    <input type="hidden" name="list_id" value="872040">
                    <input type="hidden" name="no_conf" value="">
                    <input type="hidden" name="notify" value="">
                    <div class="input-group inline-group">
                        <div class="input-group">

                            <input type="email" name="email" id="mce-EMAIL" placeholder="" required="required">
                            <label for="mce-EMAIL">Ваш e-mail</label>
                        </div>
                        <div class="input-group btn_group">
                            <button id="infocenter_btn" type="submit" id="mc-embedded-subscribe" class="btn-style">Получать новости</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="infocenter-callback__content__right-side">
                <div class="h4 block-title">Мы в социальных сетях <br> #cbsgroup</div>
                <p class="form-title">Подписывайтесь на наши профили в социальных сетях.</p>
                <p class="form-subtitle">Там мы публикуем новости, полезные материалы, разбираем кейсы и отвечаем на вопросы.</p>
                <ul class="social-list">
                    <li class="social-list__item"><a href="{'insta' | option}" target="_blank" class="social-list__link inst-link"></a></li>
                    <li class="social-list__item"><a href="{'fb' | option}" target="_blank" class="social-list__link fb-link"></a></li>
                    <li class="social-list__item"><a href="{'vk' | option}" target="_blank" class="social-list__link vk-link"></a></li>
                    <li class="social-list__item"><a href="{'tw' | option}" target="_blank" class="social-list__link tw-link"></a></li>
                    <li class="social-list__item"><a href="{'tube' | option}" target="_blank" class="social-list__link yt-link"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>