{if $service2blocks?}
    <div class="service-main__content__right-side">
        {var $blocks = $service2blocks|fromJSON}
        {foreach 0..1 as $i}
            {if $blocks[$i].bg?}
                {var $bg = 'data-bg="url('~$blocks[$i].bg~')"'}
            {else}
                {var $bg = ''}
            {/if}
            {if $blocks[$i].img?}
                {var $img = 'data-bg="url('~$blocks[$i].img~')" style="background-position:'~$blocks[$i].position~'"'}
            {else}
                {var $img = ''}
            {/if}
            <div class="service-header__right-block lazy" {$bg}>
                <div class="service-header__right-block-inner lazy" {$img}>
                    <p class="service-header__right-block-header service-header__right-block-header--{$blocks[$i].headerColor}">{$blocks[$i].title}</p>
                    <p class="service-header__right-block-descr">{$blocks[$i].descr}</p>
                    {if $blocks[$i].video == ''}
                        <div class="service-header__links service-header__links--{$blocks[$i].linkColor}">
                            {if $blocks[$i].link1?}
                                <a data-smooth href="{$blocks[$i].link1}"><span>{$blocks[$i].link1text}</span></a>
                            {/if}
                            {if $blocks[$i].link2?}
                                <a  data-smooth href="{$blocks[$i].link2}"><span>{$blocks[$i].link2text}</span></a>
                            {/if}
                        </div>
                    {else}
                        <a href="{$blocks[$i].video}" title="" class="video-link video-link-modal"></a>
                    {/if}
                </div>
            </div>
        {/foreach}
    </div>
{else}
    <div class="service-main__content__right-side"
         style="background-image: url({$_modx->resource.image | phpthumbon:"w=473&h=766&q=70&zc=1"});">
        {if $video = ($video)?:$_modx->resource.video}
            <a href="{$video}" title="{$pagetitle}" class="video-link video-link-modal"><p>Видео об услуге</p></a>
        {/if}
    </div>
{/if}