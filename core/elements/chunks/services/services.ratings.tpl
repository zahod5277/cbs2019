<div class="raiting-slider" id="raiting-slider">
    <div class="container">
        {if $raiting_slider = (4 | resource:"raiting_slider")}
            {foreach json_decode($raiting_slider, true) as $raiting_block}
                {if $raiting_block@first}

                    <div class="title-row">
                        <div class="h3 raiting-slider__title">{$raiting_block.title}</div>
                        <div class="raiting-slider__btns">
                            <div class="answers-count">
                                <div class="slide-val">
                                    <span class="current-slide"></span>
                                    <span class="all-slides"></span>
                                </div>
                                <div class="slide-btns">
                                    <button type="button" role="presentation" id="award-prev" class="owl-prev">
                                        <i class="icon icon-s-left-arrow"></i>
                                    </button>
                                    <button type="button" role="presentation" id="award-next" class="owl-next">
                                        <i class="icon icon-s-right-arrow"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="raiting-slider__content">
                        <div class="raiting-slider__content__left-side">
                            <p class="h6 slide-title">{$raiting_block.subtitle}</p>
                            <div class="slide-info">
                                {$raiting_block.desc}
                            </div>
                            <div class="top-block">
                                <div class="top-block__left-side">
                                    <p class="top-title"><strong>ТОП 100</strong> компании России</p>
                                    <ul>
                                        <li>Аудит</li>
                                        <li>Бухучет</li>
                                        <li>Консалтинг</li>
                                    </ul>
                                </div>
                                <div class="top-block__right-side">
                                    <img data-src="{'assets/tpl/images/rating-logo-image.jpg'|image_optimize}" alt="Исключительно высокий уровень надежности" class="lazy top-image">
                                    <div class="top-descr">
                                        <p>Исключительно высокий уровень надежности</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {set $images = json_decode($raiting_block.images, true)}
                        <div class="raiting-slider__content__right-side">
                            <div id="awards-slider" class="owl-carousel awards-slider">
                                <div class="slide-item">
                                    {foreach $images as $img index=$img_idx}
                                        <a href="{$img.img}" title="{$img.name}" class="image-block">
                                            <img data-src="{$img.img | phpthumbon:"w=310&h=232&bg=ffffff&q=70"|image_optimize}" alt="{$img.name}" class="lazy link-image">
                                        </a>
                                        {if (($img_idx+1) % 4 === 0)}
                                        </div>
                                        <div class="slide-item">
                                        {/if}
                                    {/foreach}
                                </div>
                            </div>
                        </div>
                    </div>
                {/if}
            {/foreach}
        {/if}
    </div>
</div>