<div class="service-branches">
    <div class="container">
        <div class="h3 service-branches__title">Отрасли, в которых мы сильны</div>
        <p class="h6 service-branches__subtitle">Наши клиенты — компании, работающие в данных отраслях. Мы понимаем специфику и знаем на что обратить внимание.</p>
        <div class="service-branches__content">
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/commerce.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Торговля</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/manufacture.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Производство</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/construction1.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Cтроительство</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/communal.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>ТСЖ, ЖСК, ТСН</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/logistics.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Транспорт <br> и логистика</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/fitness.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Фитнес клубы</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/car-service.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Автосервисы</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/rental.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Аренда</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/non-profit.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Некоммерческие организации</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/services.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Услуги и сервис</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/software.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Софт и IT</p>
                </div>
            </div>
            <div class="service-branches__content__block">
                <div class="image-block"><img data-src="assets/tpl/images/svg/branches/pharmacy.svg" alt="icon-image" class="lazy icon-block"></div>
                <div class="descr-block">
                    <p>Фармацевтика</p>
                </div>
            </div>
        </div>
    </div>
</div>