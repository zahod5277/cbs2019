{if $_modx->resource.advisable?}
    {var $res = $_modx->resource.advisable}
{else}
    {var $res = $_modx->resource.parent|resource:'advisable'}
{/if}
{if $res?}
    {$_modx->runSnippet("pdoResources", [
        "fastMode" => 1,
        "includeContent" => 1,
        "includeTVs" => "image,tags",
        "resources" => $res,
        "parents" => 0,
        "templates" => 9,
        "tpl" => '@FILE:chunks/services/services.recommended.reading.row.tpl',
        "tplWrapper" => '@FILE:chunks/services/services.recommended.reading.outer.tpl',
        "limit" => 30,
        "showLog" => 0,
    ])}
{/if}