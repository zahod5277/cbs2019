<div class="inline-block {$block.color_background}">
    <div class="container">
        <div class="inline-block__label">
            <div class="inline-block__overlay-ribbon">
                <div class="inline-block__ribbon-content">
                    <h3 class="inline-block__ribbon-title">Антикризисное</h3>
                    <h4 class="inline-block__ribbon-title inline-block__ribbon-title--small">предложение!</h4>
                </div>
            </div>
        </div>
        <div class="inline-block__content">
            <h3 class="h3 inline-block__title">{$block.title}</h3>
            <h4 class="h4 inline-block__subtitle">{$block.subtitle}</h4>
            <div class="inline-block__descr">
                {$block.desc}
            </div>
            <a href="{$block.link|url}" class="inline-block__btn">{$block.button}</a>
        </div>
    </div>
</div>