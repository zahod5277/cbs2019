<div class="service-callback">
    <div class="container">
        <div class="service-callback__content">
            <div class="service-callback__content__left-side">
                <div class="h3 service-callback__title">Остались вопросы?</div>
                <p class="lside-descr">Мы часто сомневаемся. У нас часто  остается недосказанность. Я тоже человек и делаю свой выбор осторожно и расчетливо. Но не всегда имею возможность воспользоваться советом. Воспользуйтесь нашим советом. Это бесплатно и безопасно.</p>
                <div class="person-block">
                    <div class="image-block">
                        <img data-src="{$serv_how_photo | rezimgcrop:"r-150x,c-120x120"|image_optimize}" alt="{$serv_how_fio}" class="lazy person-image">
                    </div>
                    <div class="descr-block">
                        <p class="name">{$serv_how_fio}</p>
                        <p class="who">{$serv_how_rang}</p>
                    </div>
                </div>
            </div>
            <div class="service-callback__content__right-side">
                <div class="h3 service-callback__title">Заказать обратный звонок </div>
                <p class="form-title">Оставьте свой номер и наш менеджер свяжется с вами в течение 5 минут.</p>
                {'!AjaxForm' | snippet : [
                    'frontend_css' => 0,
                    'snippet' => 'FormIt',
                    'hooks' => 'spam,csrf,modBitrixLead,FormItSaveForm',
                    'form' => 'question_form_tpl',
                    'formSelector' => 'question_form',
                    'emailTo' => $_modx->config['e-mail'],
                    'emailFrom' => "noreply@{$_modx->config.http_host}",
                    'emailSubject' => 'Обратный звонок '~$_modx->config.http_host,
                    'validate' => 'phone_f:required,email:blank',
                    'emailTpl' => 'message-discounts',
                    'btxFieldnames' => '{"phone_f":"PHONE","name_f":"NAME", "title": "TITLE", "roistat": "UF_CRM_1565004282"}',
                    'formFields' => 'phone_f,name_f,email',
                    'fieldNames' => 'phone_f==Телефон,name_f==ФИО,email==SPAM',
                    'formName' => 'Форма Обратный звонок',
                    'validationErrorMessage' => 'Вы не указали свой телефон',
                    'successMessage' => 'Мы получили вашу заявку и свяжемся с вами'
                ]}
            </div>
        </div>
    </div>
</div>