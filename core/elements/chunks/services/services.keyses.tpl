<div class="cases-slider" id="cases-slider">
    <div class="container">
        <div class="title-row">
            <div class="h3 cases-slider__title">Кейсы</div>
            <div class="cases-slider__btns">
                <div class="answers-count">
                    <div class="slide-val">
                        <span class="current-slide"></span>
                        <span class="all-slides"></span>
                    </div>
                    <div class="slide-btns"></div>
                </div>
            </div>
        </div>
        <div class="service-cases__content">
            <div id="know-slider" class="owl-carousel servicecases-slider">
                {var $categories = [
                    25 => 'Аудит',
                    5962 => 'Бухучет',
                    5976 => 'Налоги',
                    5996 => 'Консалтинг',
                    6002 => 'МСФО',
                    6004 => 'Оценка',
                    41 => 'Автоматизация',
                    
                ]}
                {foreach $categories as $key => $value}
                    {if $_modx->resource.id == $key || $_modx->resource.parent == $key}
                        {var $tag = $value}
                    {/if}
                {/foreach}
                {'!pdoPage' | snippet : [
                    'element' => 'tvssResources',
                    'tv' => '32',
                    'tag' => $tag,
                    'fastMode' => 1,
                    'parents' => 121,
                    'sortby' => 'publishedon',
                    'sortdir' => 'DESC',
                    'tpl' => '@FILE:chunks/services/services.keyses.row.tpl',
                    'includeTVs' => 'image,tags',
                    'includeContent' => 1,
                    'limit' => 100,
                    'tplWrapper' => '@INLINE {$output}'
                ]}

            </div>
        </div>
        <div class="btn-row">
            <a href="{121 | url}" class="service-cases-link btn-border-style">Все кейсы</a>
        </div>
    </div>
</div>