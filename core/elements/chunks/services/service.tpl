{if $_modx->resource.service3blocks == ''}
    {var $service3blocks = $_modx->resource.parent|resource:'service3blocks'}
{else}
    {var $service3blocks = $_modx->resource.service3blocks}
{/if}

<div class="left-side-type">
    {include 'file:chunks/common/sidebar.tpl'}
    <div class="left-side-content">
        <div class="service-main">
            <div class="container">
                <div class="service-main__content">
                    <div class="service-main__content__left-side">
                        {include 'file:chunks/common/breadcrumbs.tpl'}
                        <h1 class="h2 service-main__title">{$_modx->resource.pagetitle}</h1>
                        <div class="service-main__form" data-calc-option>
                            {if $_modx->resource.top_calc?}
                                {var $variants = $_modx->resource.top_calc|fromJSON}
                                <form action="" data-calc-option>
                                    <div class="calc-form__content__column1">
                                        <div class="input-group" style="width: 424px;height: 96px;left: 146px;top: 360px;margin-bottom: 3.75em;">
                                            <p class="block-title">{$_modx->resource.calc_h}</p>
                                            <select name="activity-type" class="selopt" id="select">
                                                {foreach $variants as $variant}
                                                    <option value="{$variant.price}">{$variant.value}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-group group-inline price-input">
                                        <span class="value-title">Стоимость:</span>
                                        <span>от</span>
                                        <span class="slider-form__block_sum" id="value" name="calc">34 900</span>
                                        <span class="cur">руб.</span>
                                    </div>
                                    <div class="input-group group-inline btn-group">
                                        <a href="#client_modal" class="red-btn-style modal-win">Оставить заявку</a>

                                        <div class="service-form-info">
                                            <p>Устраивает стоимость?</p>
                                            <p>Оставьте заявку, чтобы ее зафиксировать.</p>
                                        </div>
                                    </div>
                                </form>
                            {/if}
                        </div>
                    </div>
                    {$_modx->getChunk('@FILE:chunks/services/services.2blocks.tpl',[
                        'service2blocks' => $service3blocks
                    ])}
                </div>

            </div>
        </div>
        {if $service_block = $_modx->resource.service_block}
            {foreach json_decode($service_block, true) as $i => $serv_block}
                <div class="{$serv_block.color_background}">
                    <div class="container">
                        <div class="{$serv_block.color_background}__content">
                            <div class="{$serv_block.color_background}__content__left-side">
                                {if $i == 0}
                                    <h2 class="h3 {$serv_block.color_background}__title">{$serv_block.title}</h2>
                                    {else}
                                    <h3 class="h3 {$serv_block.color_background}__title">{$serv_block.title}</h3>
                                {/if}
                                <p class="h6 {$serv_block.color_background}__subtitle">{$serv_block.subtitle}</p>
                                <div class="{$serv_block.color_background}__descr">
                                    {$serv_block.desc}
                                </div>
                            </div>
                            {if $serv_block.slogan}
                                <div class="service-features__content__right-side">
                                    <div class="service-info">
                                        <p class="h6 block-title">{$serv_block.slogan}</p>
                                        <div class="block-descr">
                                            <p>{$serv_block.slogan_desc}</p>
                                        </div>
                                    </div>
                                    <div class="lazy service-image" data-bg="url({$serv_block.img | phpthumbon:"w=473&h=410&zc=1&q=70"})"></div>
                                </div>
                            {else}
                                {$_modx->getChunk('@FILE:chunks/services/services.1blocks.tpl',[
                                    'serviceblocks' => $service3blocks
                                ])}
                            {/if}
                        </div>
                    </div>
                </div>
            {/foreach}
        {/if}

        {if $_modx->resource.inlineBanner?}
            {var $res = $_modx->resource.inlineBanner|fromJSON}
        {else}
            {var $res = $_modx->resource.parent|resource:'inlineBanner'|fromJSON}
        {/if}
        {if $res != '' && $res[0]['active'] == 1}
            {$_modx->getChunk('@FILE:chunks/services/service.serv.inline.blocks.tpl',[
                'block' => $res[0]
            ])}
        {/if}

        {include 'file:chunks/services/services.keyses.tpl'}
        {if $service_how = $_modx->resource.service_how}
            {foreach json_decode($service_how, true) as $serv_how}
                {if $serv_how@first}
                    <div class="service-how">
                        <div class="container">
                            <div class="service-how__content">
                                <div class="service-how__content__left-side">
                                    <h3 class="h3 service-how__title">{$serv_how.title}</h3>
                                    <div class="service-features__descr">{$serv_how.desc}</div>
                                    {if $serv_how_items = $serv_how.items}
                                        {foreach json_decode($serv_how_items, true) as $serv_how_item index=$idx}
                                            <div class="service-how__row">
                                                <div class="icon-block"><span class="number">{if ($idx+1)<10}0{/if}{$idx+1}</span></div>
                                                <div class="descr-block">
                                                    {$serv_how_item.text}
                                                </div>
                                            </div>
                                        {/foreach}
                                    {/if}
                                    <div class="btn-row">
                                        <a href="#client_modal" class="red-btn-style modal-win service-how-btn">Стать клиентом</a>
                                    </div>
                                </div>
                                <div class="service-how__content__right-side">
                                    <div class="how-image-block">
                                        <img data-src="{set $serv_how_photo = $serv_how.photo | phpthumbon:"w=473&h=427&q=70&zc=T"}{$serv_how_photo}" alt="{$serv_how.fio}" class="how-work-image lazy">
                                    </div>
                                    <div class="how-block">
                                        <p class="person-name">{set $serv_how_fio = $serv_how.fio}{$serv_how_fio}</p>
                                        <p class="person-who">{set $serv_how_rang = $serv_how.rang}{$serv_how_rang}</p>
                                        <div class="person-data">
                                            <a href="tel:{$serv_how.phone?:('phone1' | option)}" class="phone-link">{$serv_how.phone?:('phone1' | option)}</a>
                                            <a href="mailto:{$serv_how.email?:('email' | option)}" class="mail-link">{$serv_how.email?:('email' | option)}</a>
                                            <a href="{$_modx->resource.uri}#spersons-slider" class="other-persons">Другие специалисты</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {/if}
            {/foreach}
        {/if}
        {if $service_what = $_modx->resource.service_what}
            {foreach json_decode($service_what, true) as $serv_what}
                {if $serv_what@first}
                    <div class="service-what">
                        <div class="container">
                            <div class="service-what__content">
                                <div class="service-what__content__left-side">
                                    <h3 class="h3 service-what__title">{$serv_what.title}</h3>
                                    <p class="h6 service-what__subtitle">{$serv_what.subtitle}</p>
                                    <div class="project-cost">
                                        <div class="project-info">
                                            {$serv_what.desc}
                                        </div>
                                        <p class="h6">{$serv_what.name_list}</p>
                                        {if $serv_what_items = $serv_what.items}
                                            <ul>
                                                {foreach json_decode($serv_what_items, true) as $serv_what_item index=$idx}
                                                    <li>{$serv_what_item.text}</li>
                                                    {/foreach}
                                            </ul>
                                        {/if}
                                        <div class="btn-row">
                                            <a href="#client_modal" class="project-cost__btn  red-btn-style modal-win">Стать клиентом</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="service-what__content__right-side">
                                    <div class="calc-block">
                                        <h3 class="h4 calc-block__title">{$serv_what.slogan}</h3>
                                        <p class="calc-block__subtitle">{$serv_what.slogan_desc}</p>
                                        <a href="{7 | url}" class="calc-block__btn btn-style white-style">Узнать стоимость</a>
                                    </div>
                                    <div class="image-block lazy" data-bg="url({$serv_what.img | phpthumbon:"w=473&h=758&q=70&zc=1"})"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                {/if}
            {/foreach}
        {/if}
        {if $advantage = $_modx->resource.advantage_main}
            {foreach json_decode($advantage, true) as $advantage_block}
                {if $advantage_block@first}
                    <div class="service-advantage">
                        <div class="container">
                            <div class="h3 service-advantage__title">{$advantage_block.title}</div>
                            <div class="service-advantage__content">
                                {foreach json_decode($advantage_block.items, true) as $adv_item}
                                    <div class="service-advantage__content__block">
                                        <div class="icon-block">
                                            <img data-src="{$adv_item.img}" alt="{$adv_item.title}" class="lazy icon-image">
                                        </div>
                                        <div class="descr-block">
                                            <p><strong>{$adv_item.title}</strong></p>
                                            <div class="descr-info">
                                                <p>{$adv_item.desc}</p>
                                            </div>
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                    </div>
                {/if}
            {/foreach}
        {/if}
        {include 'file:chunks/main/main.clients.tpl'}
        {include 'file:chunks/reviews/reviews.answer.slider.tpl'}
        {include 'file:chunks/services/services.ratings.tpl'}
        
        {include 'file:chunks/services/services.recommended.reading.tpl'}
        
        {if $about_keypersons = $_modx->resource.about_keypersons}
            {foreach json_decode($about_keypersons, true) as $keypersons_block}
                {if $keypersons_block@first}
                    <div class="service-persons">
                        <div class="container">
                            <div class="title-row">
                                <div class="h3 cases-slider__title">{$keypersons_block.title}</div>
                                <div class="cases-slider__btns">
                                    <div class="answers-count">
                                        <div class="slide-val">
                                            <span class="current-slide"></span>
                                            <span class="all-slides"></span>
                                        </div>
                                        <div class="slide-btns"></div>
                                    </div>
                                </div>
                            </div>
                            <p class="h6 cases-slider__subtitle">Хорошие специалисты – залог хорошей работы</p>    
                            {if $personals = json_decode($keypersons_block.personals, true)}
                                <div class="service-persons__content">
                                    <div id="spersons-slider" class="spersons-slider owl-carousel">
                                        {foreach $personals as $persona index=$pers_idx}
                                            <div class="slide-item">
                                                <div class="image-block">
                                                    <img data-src="{$persona.photo | phpthumbon:"w=292&h=347&zc=T&q=70"|image_optimize}" alt="{$persona.fio}" class="person-image lazy">
                                                </div>
                                                <div class="descr-block">
                                                    <p class="person-name">{$persona.fio}</p>
                                                    <p class="person-who">{$persona.rang}</p>
                                                    <p class="small">{$persona.desc | notags}</p>
                                                </div>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            {/if}
                        </div>
                    </div>
                {/if}
            {/foreach}
        {/if}
        {if $_modx->resource.parent == 5962 || $_modx->resource.id == 5962}
            {include 'file:chunks/services/services.branches.buh.tpl'}
        {else}
            {include 'file:chunks/services/services.branches.tpl'}
        {/if}
        {include 'file:chunks/services/services.callback.tpl'}
    </div>
</div>
