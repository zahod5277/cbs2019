<div class="cases-slider">
    <div class="container">
        <div class="title-row">
            <div class="h3 cases-slider__title">Полезно знать</div>
            <div class="cases-slider__btns">
                <div class="answers-count">
                    <div class="slide-val">
                        <span class="current-slide"></span>
                        <span class="all-slides"></span>
                    </div>
                    <div class="slide-btns"></div>
                </div>
            </div>
        </div>
        <div class="service-cases__content">
            <div id="recommended-slider" class="owl-carousel servicecases-slider">{$output}</div>
        </div>
        <div class="btn-row">
            <a href="{13 | url}" class="service-cases-link btn-border-style">Все статьи</a>
        </div>
    </div>
</div>