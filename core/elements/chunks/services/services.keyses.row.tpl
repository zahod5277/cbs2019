<div class="slide-item">
    <div class="case-block">
        <a href="{$uri}" title="{$_pls["image"]["altTag"]?:$pagetitle | htmlent}">
            <div class="image-block">
                 {if $image}
                <div class="lazy block-background" data-bg="url('{$image["sourceImg"]["src"] | phpthumbon:"w=422&h=183&q=70&zc=C"|image_optimize}')">
                </div>
                {/if}
            </div>
            <div class="descr-block">
                <p class="block-title">{$pagetitle}</p>
                {$introtext?:"<p>"~($content | notags | stripmodxtags | truncate : 80 : "..." : true : true)~"</p>"}
            </div>
            <div class="info-block">
                <span class="category">{($parent | resource:"menutitle")?:($parent | resource:"pagetitle")}{if $_pls["tv.tags"]}, 
                    {foreach ($_pls["tv.tags"]|split) as $tag last=$last}
                    {if $last}{$tag}{else}{$tag}, {/if}
                {/foreach}
                {/if}</span>
                <span class="date">{$publishedon | dateago:"{\"dateNow\":0, \"dateFormat\":\"d F Y\", \"dateDay\":\"d F Y\", \"dateHours\":0}"}</span>
            </div>
    </div>
</a>
</div>