<div class="slide-item">
    <div class="case-block recommened-block{if $_pls["tv.image"]["src"]==''} recommened-block--text{/if}">
        <a href="{$uri}" title="{$_pls["tv.image"]["altTag"]?:$pagetitle | htmlent}">
            <div class="image-block">
                {if $_pls["tv.image"]["sourceImg"]["src"]?}
                <div class="lazy block-background"data-bg="url('{$_pls["tv.image"]["sourceImg"]["src"] | phpthumbon:"w=399&h=173&q=70&zc=1"}')">
                </div>
                {/if}
                {*<img src="{$_pls["tv.image"]["sourceImg"]["src"] | phpthumbon:"w=399&h=173&q=70&zc=1"}" alt="{$_pls["tv.image"]["altTag"]?:$pagetitle | htmlent}" class="service-case-image">*}
            </div>
            <div class="descr-block">
                <p class="block-title">{$pagetitle}</p>
                {$introtext?:"<p>"~($content | notags | stripmodxtags | truncate : 80 : "..." : true : true)~"</p>"}
            </div>
            <div class="info-block">
                <span class="category">{($parent | resource:"menutitle")?:($parent | resource:"pagetitle")}{if $_pls["tv.tags"]}, 
                    {foreach ($_pls["tv.tags"]|split) as $tag last=$last}
                    {if $last}{$tag}{else}{$tag}, {/if}
                {/foreach}
                {/if}</span>
                <span class="date">{$publishedon | dateago:"{\"dateNow\":0, \"dateFormat\":\"d F Y\", \"dateDay\":\"d F Y\", \"dateHours\":0}"}</span>
            </div>
        </a>
    </div>
</div>