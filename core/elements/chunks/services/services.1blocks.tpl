{if $serviceblocks?}
{var $blocks = $serviceblocks|fromJSON}
    {if $blocks[2]?}
    <div class="lazy service-main__content__right-side service-know__content__right-side" data-bg="url('{$blocks[2].bg | phpthumbon:"w=473&h=727&zc=1&q=70"}')">
        <div class="lazy service-header-video-link-bg" data-bg="url('/assets/tpl/images/service-video-btn-bg2.jpg')">
            <a href="{$blocks[2].video}" title="" class="video-link video-link-modal">
            <p>{$blocks[2].title}</p>
        </a>
        </div>
    </div>
    {/if}
{/if}