<div class="{$serv_block.color_background}">
    <div class="container">
        <div class="{$serv_block.color_background}__content">
            <div class="{$serv_block.color_background}__content__left-side">
                {if $i == 0}
                    <h2 class="h3 {$serv_block.color_background}__title">{$serv_block.title}</h2>
                {else}
                    <h3 class="h3 {$serv_block.color_background}__title">{$serv_block.title}</h3>
                {/if}
                <p class="h6 {$serv_block.color_background}__subtitle">{$serv_block.subtitle}</p>
                <div class="{$serv_block.color_background}__descr">
                    {$serv_block.desc}
                </div>
            </div>
            {if $serv_block.slogan}
                <div class="service-features__content__right-side">
                    <div class="service-info">
                        <p class="h6 block-title">{$serv_block.slogan}</p>
                        <div class="block-descr">
                            <p>{$serv_block.slogan_desc}</p>
                        </div>
                    </div>
                    <div class="lazy service-image" data-bg="url({$serv_block.img | phpthumbon:"w=473&h=410&zc=1&q=70"})"></div>
                </div>
            {else}
                {$_modx->getChunk('@FILE:chunks/services/services.1blocks.tpl',[
                'serviceblocks' => $service3blocks
                ])}
            {/if}
        </div>
    </div>
</div>