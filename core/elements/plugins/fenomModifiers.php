<?php
$root = rtrim($modx->config['base_path'], '/');

require_once $root . '/vendor/autoload.php';

use Intervention\Image\ImageManager;

switch ($modx->event->name) {
    case 'pdoToolsOnFenomInit':
        /** @var Fenom $fenom
         * Мы получаем переменную $fenom при его первой инициализации и можем вызывать его методы.
         */

        /**
         * Современные форматы изображений
         */
        $fenom->addModifier('image_optimize', function ($fileUrl) use ($modx, $root) {
            $webpSupport = strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false;
            $file = $root . '/' . trim(urldecode($fileUrl), '/');
            $webpFile = $file . '.webp';
            $webpUrl = $fileUrl . '.webp';
            $allowedImages = ['png', 'jpg', 'jpeg'];
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

            if (!$webpSupport || !in_array($ext, $allowedImages))
                return $fileUrl;

            if (!file_exists($file)) {
                $modx->log(1, "image_optimize: $file not exists");
                return $fileUrl;
            }

            if (file_exists($webpFile))
                return $webpUrl;

            try {
                $manager = new ImageManager();
                $manager->make($file)->encode('webp')->save($webpFile);
            } catch (\Exception $e) {
                $modx->log(1, "image_optimize: $file " . $e->getMessage());
                return $fileUrl;
            }

            return $webpUrl;
        });

        break;
}
